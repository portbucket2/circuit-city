﻿using UnityEngine;

public class UIStateController : MonoBehaviour
{

    #region Public Variables

    public static UIStateController Instance;

    public UIMainMenuController UIMainMenuControllerReference;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
        }
        else {

            Destroy(gameObject);
        }
    }

    #endregion

    #region Public Callback

    public void AppearMainMenu() {

        UIMainMenuControllerReference.AppearManinMenu();
    }

    public void DisappearMainMenu() {

        UIMainMenuControllerReference.DisappearMainMenu();
    }

    #endregion

}
