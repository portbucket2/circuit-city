﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIMainMenuController : MonoBehaviour
{
    #region Public Variables

    [Header("Reference  :   External")]
    public LevelManager levelManagerReference;

    [Space(5.0f)]
    public Animator animatorReference;

    [Space(5.0f)]
    public TextMeshProUGUI levelTextReference;

    [Space(5.0f)]
    public Button startButtonReference;

    #endregion

    #region Private Variables

    private bool m_IsMainMenuShowing;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_IsMainMenuShowing = true;

        startButtonReference.onClick.AddListener(delegate
        {
            if (m_IsMainMenuShowing) {

                DisappearMainMenu();
                GameplayController.Instance.PreProcess();
            }
        });
    }

    private void Start()
    {
        UpdateLevelTextReference();
    }

    #endregion

    #region Configuretion

    private void UpdateLevelTextReference()
    {
        int t_CurrentLevel = levelManagerReference.GetCurrentLevel();
        levelTextReference.text = "LEVEL : " + (t_CurrentLevel + 1);
    }

    #endregion

    #region Public Callback

    public void AppearManinMenu() {

        UpdateLevelTextReference();
        animatorReference.SetTrigger("APPEAR");

        m_IsMainMenuShowing = !m_IsMainMenuShowing;
    }

    public void DisappearMainMenu() {

        animatorReference.SetTrigger("DISAPPEAR");

        m_IsMainMenuShowing = !m_IsMainMenuShowing;
    }

    #endregion

}
