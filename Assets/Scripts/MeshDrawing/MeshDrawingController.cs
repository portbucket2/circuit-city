﻿using UnityEngine;
using System.Collections.Generic;
using com.faithstudio.Math;
using com.faithstudio.Mesh;
using com.faithstudio.Gameplay;

public class MeshDrawingController : MonoBehaviour
{
    #region Public Varaibles

    [Space(5.0f)]
    [Header("Debug Reference")]

    [Space(5.0f)]
    [Header("Reference : External")]
    public Camera               cameraReference;
    public BasicMeshGenerator   basicMeshGeneratorReference;
    public LineRenderer         lineRendererReference;

    [Space(5.0f)]
    [Range(0.01f,1f)]
    public float samplerDistance = 0.01f;

    [Range(0, 5f)]
    public float impactArea;
    public Vector3 meshDeformDirection;
    public MeshFilter meshFiltererForDeformation;

    #endregion

    #region Private Variables

    private MathFunction    m_MathFunctionReference;

    private Mesh            m_MeshReference;
    private Transform       m_MeshTransformReference;

    private bool            m_IsMeshDeformationControllerRunning;
    private bool            m_IsPressed;
    private int             m_NumberOfVertices;
    private float           m_AbsoluteImpactArea;
    private Vector3         m_TouchPointOfRayCast;
    private Vector3         m_MeshPosition;

    private Ray             m_Ray;
    private RaycastHit      m_RayCastHit;

    private DeformedVertexPoint[] m_DeformedVertexPointReference;

    private List<int> m_ListOfIndexesOfDeformed;

    #endregion

    #region Mono Behaviour

    private void Start()
    {
        m_MathFunctionReference = MathFunction.Instance;

        //Debug
        PreProcess();
    }

    private void Update()
    {
        if (m_IsPressed) {

            m_Ray = cameraReference.ScreenPointToRay(m_TouchPointOfRayCast);

            if (Physics.Raycast(m_Ray, out m_RayCastHit))
            {
                if (m_RayCastHit.collider != null)
                {
                    m_MeshPosition = m_RayCastHit.transform.position;
                    ApplyMeshDeformation(m_RayCastHit.point);
                }

            }
        }
    }

    #endregion

    #region Configuretion   :   Touch Input

    private void OnTouchDown(Vector3 t_TouchPosition)
    {

        m_ListOfIndexesOfDeformed = new List<int>();

        m_TouchPointOfRayCast = t_TouchPosition;

        m_IsPressed = true;
    }

    private void OnTouch(Vector3 t_TouchPosition)
    {

        m_TouchPointOfRayCast = t_TouchPosition;

    }

    private void OnTouchUp(Vector3 t_TouchPosition)
    {
        m_IsPressed = false;

        CreateShape();
    }

    #endregion

    #region Configuretion   :   Mesh Deformation

    private void CalculateVertices()
    {
        MeshFilter t_MeshFilterReference;
        if (meshFiltererForDeformation == null)
        {
            t_MeshFilterReference = basicMeshGeneratorReference.GetMeshFilter();
            m_DeformedVertexPointReference = basicMeshGeneratorReference.GetArraysOfDeformedVertexPoint();
            m_NumberOfVertices = t_MeshFilterReference.mesh.vertices.Length;
        }
        else
        {
            t_MeshFilterReference = meshFiltererForDeformation;
            m_NumberOfVertices = m_MeshReference.vertices.Length;
            m_DeformedVertexPointReference = new DeformedVertexPoint[m_NumberOfVertices];
            for (int i = 0; i < m_NumberOfVertices; i++)
            {
                m_DeformedVertexPointReference[i] = new DeformedVertexPoint(
                        i,
                        Vector2Int.zero, // Need To Fix Later
                        m_MeshReference.vertices[i]
                    );
            }

        }
        m_MeshReference = t_MeshFilterReference.mesh;
        m_MeshTransformReference = t_MeshFilterReference.transform;

        m_AbsoluteImpactArea = (m_MeshTransformReference.localScale.x + m_MeshTransformReference.localScale.y + m_MeshTransformReference.localScale.z) * impactArea;

    }

    private Vector3 GetWorldPositionOfVertex(Vector3 t_VertexPosition)
    {

        return m_MeshTransformReference.position + new Vector3(
                m_MeshTransformReference.localScale.x * t_VertexPosition.x,
                m_MeshTransformReference.localScale.y * t_VertexPosition.y,
                m_MeshTransformReference.localScale.z * t_VertexPosition.z
            );
    }

    private void ApplyMeshDeformation(Vector3 t_InputPoint)
    {
        Vector3[] t_NewVertices = m_MeshReference.vertices;
        
        for (int i = 0; i < m_NumberOfVertices; i++)
        {

            if (m_DeformedVertexPointReference[i].IsDeformationAllowed()
            && Vector3.Distance(t_InputPoint, GetWorldPositionOfVertex(m_DeformedVertexPointReference[i].GetCurrentVertexPosition())) <= m_AbsoluteImpactArea)
            {
                //Debug.Log("Took : " + i);
                m_DeformedVertexPointReference[i].DisableDeformation();
                m_ListOfIndexesOfDeformed.Add(i);
            }

            //Previous Code : Deformation
            //if (Vector3.Distance(t_InputPoint, GetWorldPositionOfVertex(m_DeformedVertexPointReference[i].GetCurrentVertexPosition())) <= m_AbsoluteImpactArea)
            //{
            //    t_NewVertices[i] = m_DeformedVertexPointReference[i].GetDeformedPositionFromInitialPosition(meshDeformDirection, true);
            //}
        }

        m_MeshReference.vertices = t_NewVertices;

        m_MeshReference.RecalculateBounds();
        m_MeshReference.RecalculateNormals();
        m_MeshReference.RecalculateTangents();
    }

    private List<Vector3> GetSamplerDistanceFromLineRenderer() {

        List<Vector3> t_Result = new List<Vector3>();

        int t_NumberOfPositionInLineRenderer = lineRendererReference.positionCount;
        for (int i = 0; i < t_NumberOfPositionInLineRenderer; i++) {

            t_Result.Add(lineRendererReference.GetPosition(i));
        }

        return t_Result;

    }

    private List<Vector3> GetSamplerDistanceFromMesh()
    {

        List<Vector3> t_Result = new List<Vector3>();

        int t_NumberOfOuterVertices = m_ListOfIndexesOfDeformed.Count;
        int t_SamplerDistance       = Mathf.CeilToInt(t_NumberOfOuterVertices * samplerDistance);
        int t_NextVertexIndex;

        for (int index = 0; index < t_NumberOfOuterVertices; index += t_SamplerDistance)
        {

            t_NextVertexIndex = index + t_SamplerDistance;

            if (t_NextVertexIndex >= t_NumberOfOuterVertices)
                t_NextVertexIndex = t_NumberOfOuterVertices - 1;

            t_Result.Add(GetWorldPositionOfVertex(m_DeformedVertexPointReference[m_ListOfIndexesOfDeformed[t_NextVertexIndex]].GetCurrentVertexPosition()));

        }

        return t_Result;

    }

    private void CreateShape() {

        List<Vector3> t_SampledPoint= GetSamplerDistanceFromLineRenderer();

        int t_NumberOfSampledPoint  = t_SampledPoint.Count;

        Vector3 t_MidPoint        = Vector3.zero;
        Vector3 t_SelectedVertex;

        for (int i = 0; i < t_NumberOfSampledPoint; i++) {

            t_MidPoint += t_SampledPoint[i];
        }
        t_MidPoint /= t_NumberOfSampledPoint;

        Vector3[] t_NewVertices = m_MeshReference.vertices;

        for (int samplerIndex = 0; samplerIndex < t_NumberOfSampledPoint - 1; samplerIndex++) {

            

            for (int vertexIndex = 0; vertexIndex < m_NumberOfVertices; vertexIndex++)
            {
                t_SelectedVertex = GetWorldPositionOfVertex(m_DeformedVertexPointReference[vertexIndex].GetCurrentVertexPosition());
                t_SelectedVertex.y = t_MidPoint.y;
                

                if (m_MathFunctionReference.IsPointInsideTriangle(
                    new Vector2(t_MidPoint.x, t_MidPoint.z),
                    new Vector2(t_SampledPoint[samplerIndex].x, t_SampledPoint[samplerIndex].z),
                    new Vector2(t_SampledPoint[samplerIndex + 1].x, t_SampledPoint[samplerIndex + 1].z),
                    new Vector2(t_SelectedVertex.x, t_SelectedVertex.z))){

                    //Debug.Log(t_MidPoint + " : " + t_SampledPoint[samplerIndex] + " : " + t_SampledPoint[samplerIndex + 1] + " -> " + t_SelectedVertex);

                    t_NewVertices[vertexIndex] = m_DeformedVertexPointReference[vertexIndex].GetDeformedPositionFromInitialPosition(meshDeformDirection, true);
                }
            }
        }

        m_MeshReference.vertices = t_NewVertices;

        m_MeshReference.RecalculateBounds();
        m_MeshReference.RecalculateNormals();
        m_MeshReference.RecalculateTangents();

    }

    #endregion

    #region Public Callback

    public void PreProcess() {

        if (!m_IsMeshDeformationControllerRunning) {

            CalculateVertices();

            m_IsMeshDeformationControllerRunning = true;

            GlobalTouchController.Instance.OnTouchDown  += OnTouchDown;
            GlobalTouchController.Instance.OnTouch      += OnTouch;
            GlobalTouchController.Instance.OnTouchUp    += OnTouchUp;

        }
    }
    
    public void PostProcess()
    {
        m_IsMeshDeformationControllerRunning = false;

        GlobalTouchController.Instance.OnTouchDown      -= OnTouchDown;
        GlobalTouchController.Instance.OnTouch          -= OnTouch;
        GlobalTouchController.Instance.OnTouchUp        -= OnTouchUp;
    }

    #endregion
}
