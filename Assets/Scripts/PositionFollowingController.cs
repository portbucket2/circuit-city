﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PositionFollowingController : MonoBehaviour
{
    #region Public Variables

    public Transform        defaultFollowingObject;
    public List<Transform>  defaultPositionFollowingObject;

    [Space(5.0f)]
    public bool isPositionFollowingControllerStartOnEnable;
    public bool isRotateOverThePoint;

    [Space(5.0f)]
    [Range(0f,25f)]
    public float defaultForwardVelocity = 1f;
    [Range(0f, 1f)]
    public float defaultAngulerVelocity = 0.1f;
    [Range(0f,5f)]
    public float defaultDelayOnCompletingLoop;

    [Space(5.0f)]
    public Vector3 defaultPositionOffset;

    #endregion

    #region Private Variables

    private bool            m_IsPositionFollowingControllerRunning;
    private bool            m_IsConvertPositionFromTransformReferences;

    private int             m_CurrentIndexOfPosition;
    private int             m_NumberOfFollowingPosition;

    private float           m_AbsoluteForwardVelocity;
    private float           m_AbsoluteAngulerVelocity;
    private float           m_AbsoluteDelayAtEndOfLoop;

    private List<Transform> m_ListOfransformReferencesOfCurrentFollowingPosition;
    private Transform       m_CurrentFollowingObject;

    private List<Vector3>   m_ListOfCurrentFollowingPosition;
    private Vector3         m_CurrentPositionOffset;

    private UnityAction     OnLoopStart;
    private UnityAction     OnLoopEnd;

    #endregion

    #region Mono Behaviour

    private void OnEnable()
    {
        if (isPositionFollowingControllerStartOnEnable) {

            StartPositionFollowingController(
                    defaultPositionFollowingObject
                );
        }
    }

    private void OnDisable()
    {
        if (isPositionFollowingControllerStartOnEnable)
        {
            StopPositionFollowingController();
        }
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForFollowingObject() {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay     = new WaitForSeconds(t_CycleLength);
        WaitForSeconds t_WaitForDelay   = new WaitForSeconds(m_AbsoluteDelayAtEndOfLoop);

        Vector3 t_TargetPosition = m_CurrentFollowingObject.position;
        Vector3 t_ModifiedPosition;
        Quaternion t_ModifiedRotation;

        while (m_IsPositionFollowingControllerRunning) {

            if (m_IsConvertPositionFromTransformReferences) {

                m_ListOfCurrentFollowingPosition[m_CurrentIndexOfPosition] = m_ListOfransformReferencesOfCurrentFollowingPosition[m_CurrentIndexOfPosition].position;
            }

            if (Vector3.Distance(m_CurrentFollowingObject.position, t_TargetPosition) <= 0.1f) {

                m_CurrentIndexOfPosition++;
                if (m_CurrentIndexOfPosition >= m_NumberOfFollowingPosition)
                {
                    m_CurrentIndexOfPosition = 0;

                    OnLoopEnd?.Invoke();
                    yield return t_WaitForDelay;
                    OnLoopStart?.Invoke();
                }

                t_TargetPosition = m_ListOfCurrentFollowingPosition[m_CurrentIndexOfPosition] + m_CurrentPositionOffset;
            }

            t_ModifiedPosition = Vector3.MoveTowards(
                    m_CurrentFollowingObject.position,
                    t_TargetPosition,
                    m_AbsoluteForwardVelocity * Time.deltaTime
                );
            m_CurrentFollowingObject.position = t_ModifiedPosition;

            if (isRotateOverThePoint) {

                t_ModifiedRotation = Quaternion.Slerp(
                        m_CurrentFollowingObject.rotation,
                        Quaternion.LookRotation(t_TargetPosition - m_CurrentFollowingObject.position),
                        m_AbsoluteAngulerVelocity
                    );
                m_CurrentFollowingObject.rotation = t_ModifiedRotation;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForFollowingObject());
    }

    #endregion

    #region Public Callback

    public void StartPositionFollowingController(
        List<Transform> t_Position, 
        Transform t_FollowingObject = null, 
        Vector3 t_PositionOffset = new Vector3(),
        int t_StartingIndex = 0,
        UnityAction OnLoopStart = null,
        UnityAction OnLoopEnd = null) {

        m_ListOfransformReferencesOfCurrentFollowingPosition    = new List<Transform>(t_Position);
        
        int             t_NumberOfFollowingPosition     = t_Position.Count;
        List<Vector3> t_ListOfCurrentFollowingPosition  = new List<Vector3>();
        for (int i = 0; i < t_NumberOfFollowingPosition; i++) {

            t_ListOfCurrentFollowingPosition.Add(m_ListOfransformReferencesOfCurrentFollowingPosition[i].position);
        }

        m_IsConvertPositionFromTransformReferences = true;
        StartPositionFollowingController(
            t_ListOfCurrentFollowingPosition, 
            t_FollowingObject, 
            t_PositionOffset, 
            t_StartingIndex,
            OnLoopStart,
            OnLoopEnd);
    }

    public void StartPositionFollowingController(
        List<Vector3> t_Position, 
        Transform t_FollowingObject = null, 
        Vector3 t_PositionOffset = new Vector3(),
        int t_StartingIndex = 0,
        UnityAction OnLoopStart = null,
        UnityAction OnLoopEnd = null) {

        m_ListOfCurrentFollowingPosition    = new List<Vector3>(t_Position);
        m_NumberOfFollowingPosition         = t_Position.Count;

        if (t_FollowingObject == null)
            m_CurrentFollowingObject = defaultFollowingObject;
        else
            m_CurrentFollowingObject = t_FollowingObject;

        if (t_PositionOffset == Vector3.zero)
            m_CurrentPositionOffset = defaultPositionOffset;
        else
            m_CurrentPositionOffset = t_PositionOffset;

        //Future Development : Accessed Through Script
        m_AbsoluteForwardVelocity = defaultForwardVelocity;
        m_AbsoluteAngulerVelocity = defaultAngulerVelocity;
        m_AbsoluteDelayAtEndOfLoop= defaultDelayOnCompletingLoop;

        m_CurrentIndexOfPosition            = t_StartingIndex;
        m_CurrentFollowingObject.position   = m_ListOfCurrentFollowingPosition[m_CurrentIndexOfPosition] + m_CurrentPositionOffset;

        this.OnLoopStart    = OnLoopStart;
        this.OnLoopEnd      = OnLoopEnd;

        if (!m_IsPositionFollowingControllerRunning)
        {
            m_IsPositionFollowingControllerRunning = true;
            StartCoroutine(ControllerForFollowingObject());
        }
    }

    public void StopPositionFollowingController() {

        m_IsConvertPositionFromTransformReferences = false;
        m_IsPositionFollowingControllerRunning = false;
    }

    #endregion
}
