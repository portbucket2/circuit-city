﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Gameplay;

public class GuidedPathGenerator : MonoBehaviour
{

    #region Custom Variables

    [System.Serializable]
    public class GuidedPath
    {
        public  Transform                    originTransformReference;
        public  Vector3                      guidedProbeDirection;

        private int m_NumberOfGuidedProbeInPath;
        [SerializeField]
        private List<GuidedProbeAttribute>   m_ListOfGuidedProb;

        public GuidedPath(Transform originTransformReference, Vector3 guidedProbeDirection) {

            this.originTransformReference   = originTransformReference;
            this.guidedProbeDirection       = guidedProbeDirection;

            m_NumberOfGuidedProbeInPath         = 0;
            m_ListOfGuidedProb                = new List<GuidedProbeAttribute>();
        }

        public void AddGuidedProbe(GuidedProbeAttribute t_GuidedProbeAttribute) {

            m_ListOfGuidedProb.Add(t_GuidedProbeAttribute);
            m_NumberOfGuidedProbeInPath++;
        }

        public void RemoveGuidedProbe(int t_Index) {

            m_ListOfGuidedProb.RemoveAt(t_Index);
            m_NumberOfGuidedProbeInPath--;
        }

        public void SetInterpolatedValueForInitialPosition(int t_IndexForGuidedProbe, float t_InterpolatedValue) {

            m_ListOfGuidedProb[t_IndexForGuidedProbe].initialInterpolatedValue = t_InterpolatedValue;
        }

        public void SetPositionForGuidedProbe(int t_IndexForGuidedProbe, Vector3 t_NewPosition)
        {
            m_ListOfGuidedProb[t_IndexForGuidedProbe].SetPosition(t_NewPosition);
        }

        public void ShowGuidedProbe(int t_IndexForGuidedProbe) {

            m_ListOfGuidedProb[t_IndexForGuidedProbe].ShowMesh();
        }

        public void HideGuidedProbe(int t_IndexForGuidedProbe) {

            m_ListOfGuidedProb[t_IndexForGuidedProbe].HideMesh();
        }

        public int GetNumberOfGuidedProbe() {

            return m_NumberOfGuidedProbeInPath;
        }

        public float GetInterpolatedValueForInitialPositionOfGuidedProbe(int t_IndexForGuidedProbe) {

            return m_ListOfGuidedProb[t_IndexForGuidedProbe].initialInterpolatedValue;
        }

        public Vector3 GetPositionOfGuidedProbe(int t_IndexForGuidedProbe) {

            return m_ListOfGuidedProb[t_IndexForGuidedProbe].GetPosition();
        }

        public GameObject GetGuidedProbe(int t_IndexForGuidedProbe) {

            return m_ListOfGuidedProb[t_IndexForGuidedProbe].gameObject;
        }

    }

    #endregion

    #region Public Variables

    [Space(5.0f)]
    [Header("Configuretion  :   GuidedProbe")]
    public bool             userInternalTouchDetecionToHideGuidedPath;
    [Range(0.5f, 0.9f)]
    public float            densityOfGuidedProbe = 0.5f;
    [Range(0f,10f)]
    public float            animationSpeed;

    [Space(5.0f)]
    public LayerMask        layerOfGuidedProbe;

    [Space(5.0f)]
    public GameObject       guidedProbePrefab;
    public GameObject       guidedProbeCornerPrefab;
    public PreLoadedPrefab  guidedProbeContainer;

    [Space(10.0f)]
    [Header("Configuretion  :   External Reference")]
    public Camera           mainCameraReference;
    
    [Space(10.0f)]
    [Header("Parameter      :   Default")]
    public List<Transform>  defaultPointForGuidedPath;

    #endregion

    #region Private Variables

    private bool m_IsGuidedPathAnimationControllerRunning;

    private int m_TotalNumberOfGuidedProbe;
    private int m_CounterForTrackingOnHittingGuidedProbe;

    private float m_VelocityOnShowingTheCompleteLine;

    private Transform m_CameraTransformReference;

    private Ray m_Ray;
    private RaycastHit m_RayCastHit;

    private UnityAction OnFollowingGuidedPathProgressed;
    private UnityAction OnFollowingGuidedPathEnd;

    private List<GuidedPath> m_ListOfGuidedPath;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_CameraTransformReference = mainCameraReference.transform;
    }

    #endregion

    #region Configuretion

    private void ExecutePreConfiguretionForGuidedPath(List<Transform> t_ListPointForGuidedPath) {

        m_CounterForTrackingOnHittingGuidedProbe = 0;
        m_TotalNumberOfGuidedProbe = 0;

        int t_NumberOfGuidedPoint = t_ListPointForGuidedPath.Count;
        List<float> t_DistanceOfGuidedPath = new List<float>();

        //Finding The Smallest Distance
        float t_MinimalDistance = Mathf.Infinity;
        for (int i = 0; i < t_NumberOfGuidedPoint - 1; i++) {

            float t_CurrentDistance = Vector3.Distance(
                t_ListPointForGuidedPath[i].position,
                t_ListPointForGuidedPath[i + 1].position);

            t_DistanceOfGuidedPath.Add(t_CurrentDistance);

            if (t_MinimalDistance > t_CurrentDistance)
                t_MinimalDistance = t_CurrentDistance;
        }

        float t_UnitDistance = t_MinimalDistance * (1f - densityOfGuidedProbe);

        m_ListOfGuidedPath = new List<GuidedPath>();

        for (int i = 0; i < t_NumberOfGuidedPoint - 1; i++) {

            int t_NumberOfGuidedProbe            = Mathf.CeilToInt(t_DistanceOfGuidedPath[i] / t_UnitDistance);
            Vector3 t_DirectionTowardGuidedProbe = Vector3.Normalize(t_ListPointForGuidedPath[i + 1].position - t_ListPointForGuidedPath[i].position);

            m_ListOfGuidedPath.Add(new GuidedPath(
                    t_ListPointForGuidedPath[i],
                    t_DirectionTowardGuidedProbe
                ));


            for (int guidedProbeIndex = 0; guidedProbeIndex < t_NumberOfGuidedProbe; guidedProbeIndex++) {

                float t_OffSet                          = (guidedProbeIndex) * t_UnitDistance;
                float t_InterpolatedPosition            = t_OffSet / t_DistanceOfGuidedPath[i];
                
                Vector3 t_NewGuidedProbePosition        = t_ListPointForGuidedPath[i].position + (t_DirectionTowardGuidedProbe * t_OffSet);

                
                GameObject t_NewGuidedProbe             = guidedProbeContainer.PullPreloadedPrefab( guidedProbeIndex == 0 ? guidedProbeCornerPrefab : guidedProbePrefab);
#if UNITY_EDITOR
                t_NewGuidedProbe.name = "Probe (" + guidedProbeIndex + ")";
#endif
                Transform t_TransformReference          = t_NewGuidedProbe.transform;
                t_TransformReference.position           = t_NewGuidedProbePosition;
                t_TransformReference.rotation           = Quaternion.LookRotation(t_DirectionTowardGuidedProbe);

                //Debug.Log(t_NewGuidedProbe.name + " : " + t_OffSet + " : " + t_InterpolatedPosition + " : " + t_NewGuidedProbePosition);

                m_ListOfGuidedPath[i].AddGuidedProbe(t_NewGuidedProbe.GetComponent<GuidedProbeAttribute>());
                m_ListOfGuidedPath[i].SetInterpolatedValueForInitialPosition(guidedProbeIndex, t_InterpolatedPosition);

                m_ListOfGuidedPath[i].ShowGuidedProbe(guidedProbeIndex);

               m_TotalNumberOfGuidedProbe++;
            }
        }

        if (animationSpeed > 0) {

            m_IsGuidedPathAnimationControllerRunning = true;
            StartCoroutine(ControllerForAnimatingGuidedPath(t_UnitDistance));
        }
    }

    private IEnumerator ControllerForAnimatingGuidedPath(float t_UnitDistance = 0) {

        float t_CycleLength         = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        Vector3 t_TargetedPosition;
        Vector3 t_CurrentPosition;
        Vector3 t_ModifiedPosition;

        int t_NumberOfGuidedProbe;
        int t_NumberOfGuidedPath = m_ListOfGuidedPath.Count;
        float t_DeltaTime;

        while (m_IsGuidedPathAnimationControllerRunning) {

            t_DeltaTime = Time.deltaTime;

            for (int i = 0; i < t_NumberOfGuidedPath; i++) {

                t_NumberOfGuidedProbe = m_ListOfGuidedPath[i].GetNumberOfGuidedProbe();
                for (int j = 0; j < t_NumberOfGuidedProbe - 1; j++) {

                    t_TargetedPosition = 
                        m_ListOfGuidedPath[i].originTransformReference.position 
                        + (m_ListOfGuidedPath[i].guidedProbeDirection * ((j + 1) * t_UnitDistance));

                    t_CurrentPosition = m_ListOfGuidedPath[i].GetPositionOfGuidedProbe(j);

                    //Debug.Log(j + " : " + m_ListOfGuidedPath[i].GetInterpolatedValueForInitialPositionOfGuidedProbe(j + 1) + " : " + t_CurrentPosition + " > " + t_TargetedPosition);

                    if (Vector3.Distance(t_CurrentPosition, t_TargetedPosition) <= 0.1f)
                    {

                        m_ListOfGuidedPath[i].SetPositionForGuidedProbe(
                            j,
                            m_ListOfGuidedPath[i].originTransformReference.position + (m_ListOfGuidedPath[i].guidedProbeDirection * (j * t_UnitDistance)));
                    }
                    else {

                        t_ModifiedPosition = Vector3.MoveTowards(
                                t_CurrentPosition,
                                t_TargetedPosition,
                                animationSpeed * t_DeltaTime
                            );

                        m_ListOfGuidedPath[i].SetPositionForGuidedProbe(
                            j,
                            t_ModifiedPosition);
                    }
                }
            }

            yield return t_CycleDelay;
        }

        yield return null;
    }

    private void CastRayOnGuidedProbe(Ray t_Ray)
    {
        //Make Sure, The Line Cast Distance > SmoothLineDrawingController
        if (Physics.Raycast(t_Ray, out m_RayCastHit, 300f, layerOfGuidedProbe))
        {

            GuidedProbeAttribute t_GuidedProbAttribute = m_RayCastHit.collider.GetComponent<GuidedProbeAttribute>();

            if (t_GuidedProbAttribute != null && !t_GuidedProbAttribute.IsMeshHidden())
            {

#if UNITY_IOS

                HapticFeedbackController.Instance.TapPopVibrate();
#endif

                t_GuidedProbAttribute.HideMesh();

                OnFollowingGuidedPathProgressed?.Invoke();

                m_CounterForTrackingOnHittingGuidedProbe++;

                //Debug.Log(m_CounterForTrackingOnHittingGuidedProbe + " : " + m_TotalNumberOfGuidedProbe);

                if (m_CounterForTrackingOnHittingGuidedProbe >= m_TotalNumberOfGuidedProbe)
                    OnFollowingGuidedPathEnd?.Invoke();
            }
        }
    }
    private void OnTouchDown(Vector3 t_TouchPosition) { CastRayOnGuidedProbeThroughTouchPosition(t_TouchPosition); }
    private void OnTouch(Vector3 t_TouchPosition) { CastRayOnGuidedProbeThroughTouchPosition(t_TouchPosition); }
    private void OnTouchUp(Vector3 t_TouchPosition) { CastRayOnGuidedProbeThroughTouchPosition(t_TouchPosition); }

    #endregion

    #region Public Callback

    public void CastRayOnGuidedProbeThroughTouchPosition(Vector3 t_TouchPosition)
    {
        CastRayOnGuidedProbe(mainCameraReference.ScreenPointToRay(t_TouchPosition));
    }

    public void CastRayOnGuidedProbeThroughWorldSpace(Vector3 t_WorldPosition)
    {
        CastRayOnGuidedProbe(new Ray(
                m_CameraTransformReference.position,
                Vector3.Normalize(t_WorldPosition - m_CameraTransformReference.position)
            ));
    }

    public void ShowGuidedPath(
        List<Transform> t_ListPointForGuidedPath = null,
        UnityAction OnFollowingGuidedPathEnd = null,
        UnityAction OnFollowingGuidedPathProgressed = null) {

        this.OnFollowingGuidedPathEnd           = OnFollowingGuidedPathEnd;
        this.OnFollowingGuidedPathProgressed    = OnFollowingGuidedPathProgressed;
        
        if (t_ListPointForGuidedPath == null)
        {
            ExecutePreConfiguretionForGuidedPath(defaultPointForGuidedPath);
        }
        else {

            ExecutePreConfiguretionForGuidedPath(t_ListPointForGuidedPath);
        }

        if (userInternalTouchDetecionToHideGuidedPath) {

            GlobalTouchController.Instance.OnTouchDown  += OnTouchDown;
            GlobalTouchController.Instance.OnTouch      += OnTouch;
            GlobalTouchController.Instance.OnTouchUp    += OnTouchUp;
        }

        

    }

    public void HideGuidedPath() {

        int t_NumberOfGuidedPath    = m_ListOfGuidedPath.Count;
        for (int i = 0; i < t_NumberOfGuidedPath; i++) {

            int t_NumberOfGuidedProbe = m_ListOfGuidedPath[i].GetNumberOfGuidedProbe();
            for (int j = 0; j < t_NumberOfGuidedProbe; j++) {

                guidedProbeContainer.PushPreloadedPrefab(m_ListOfGuidedPath[i].GetGuidedProbe(j));
            }
        }

        m_ListOfGuidedPath.Clear();

        if (userInternalTouchDetecionToHideGuidedPath) {

            GlobalTouchController.Instance.OnTouchDown  -= OnTouchDown;
            GlobalTouchController.Instance.OnTouch      -= OnTouch;
            GlobalTouchController.Instance.OnTouchUp    -= OnTouchUp;
        }
    }

    public float GetProgressionOfFollowingGuidedPath() {

        
        return m_CounterForTrackingOnHittingGuidedProbe / (1f * m_TotalNumberOfGuidedProbe);
    }

    #endregion

}
