﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Gameplay;

public class TutorialController : MonoBehaviour
{
    #region Public Variables

    [Header("Configuretion  :   External References")]

    public Transform                    cameraTransformReference;

    [Space(5.0f)]
    public LevelManager                 levelManagerReference;
    public LineDrawingController        lineDrawingControllerReference;
    public PositionFollowingController  positionFollowingControllerReferences;
    public GameplayController           gameplayControllerReference;

    [Space(10.0f)]
    [Header("Configuretion  :   Tutorial Parameter")]
    public bool         useLevelLimiter;
    [Range(1,5)]
    public int          levelLimit = 1;
    public Transform    transformReferenceoOfTutorialHand;
    public Animator     animatorReferenceForTutorialHand;

    #endregion

    #region Private Variables

    private bool m_IsTutorialHandLookRotationControllerRunning;
    [SerializeField]
    private bool m_IsUserDrawing;

    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    private void OnTouchDown(Vector3 t_TouchPosition) {

        m_IsUserDrawing = true;
        animatorReferenceForTutorialHand.SetTrigger("DISAPPEAR");
        positionFollowingControllerReferences.StopPositionFollowingController();
    }

    private void OnTouchUp(Vector3 t_TouchPosition) {

        m_IsUserDrawing = false;

        List<Transform> t_ListOfTransformReferenceOfGuidedProbeOfCurrentBuilding = gameplayControllerReference.GetListOfTransformReferenceOfGuidedProbeOfCurrentBuilding();
        Vector3 t_LatestPositionOfLineRenderer = lineDrawingControllerReference.GetLatestPositionOfLineRenderer();

        int t_NearestProbeIndex = 0;

        if (t_LatestPositionOfLineRenderer != Vector3.zero)
        {

            int t_NumberOfGuidedProbe = t_ListOfTransformReferenceOfGuidedProbeOfCurrentBuilding.Count;
            float t_CurrentDistance = Mathf.Infinity;
            float t_CalculatedDistance;

            for (int i = 0; i < t_NumberOfGuidedProbe; i++)
            {

                t_CalculatedDistance = Vector3.Distance(t_LatestPositionOfLineRenderer, t_ListOfTransformReferenceOfGuidedProbeOfCurrentBuilding[i].position);
                if (t_CalculatedDistance < t_CurrentDistance)
                {

                    t_CurrentDistance = t_CalculatedDistance;
                    t_NearestProbeIndex = i;
                }
            }
        }


        animatorReferenceForTutorialHand.SetTrigger("APPEAR");
        positionFollowingControllerReferences.StartPositionFollowingController(
            gameplayControllerReference.GetListOfTransformReferenceOfGuidedProbeOfCurrentBuilding(),
            transformReferenceoOfTutorialHand,
            Vector3.zero,
            t_NearestProbeIndex,
            delegate {
                if (!m_IsUserDrawing)
                {
                    animatorReferenceForTutorialHand.SetTrigger("APPEAR");
                }
            },
            delegate {

                animatorReferenceForTutorialHand.SetTrigger("DISAPPEAR");
            }
        );

    }

    private IEnumerator ControllerForTutorialHandLookRotation(Vector3 t_TutorialHandPivot = new Vector3()) {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        Quaternion t_ModifiedRotation;

        while (m_IsTutorialHandLookRotationControllerRunning) {

            t_ModifiedRotation = Quaternion.Slerp(
                    transformReferenceoOfTutorialHand.rotation,
                    Quaternion.LookRotation(cameraTransformReference.position - t_TutorialHandPivot),
                    0.2f
                );
            transformReferenceoOfTutorialHand.rotation = t_ModifiedRotation;

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForTutorialHandLookRotation());
    }

    #endregion

    #region Public Callback

    public void ShowTutorial(Vector3 t_TutorialHandPivot = new Vector3()) {

        if (!useLevelLimiter || levelManagerReference.GetCurrentLevel() < levelLimit) {

            if (!m_IsTutorialHandLookRotationControllerRunning) {

                m_IsTutorialHandLookRotationControllerRunning = true;
                StartCoroutine(ControllerForTutorialHandLookRotation(t_TutorialHandPivot));
            }

            GlobalTouchController.Instance.OnTouchDown  += OnTouchDown;
            GlobalTouchController.Instance.OnTouchUp    += OnTouchUp;

            animatorReferenceForTutorialHand.SetTrigger("APPEAR");
            positionFollowingControllerReferences.StartPositionFollowingController(
                    gameplayControllerReference.GetListOfTransformReferenceOfGuidedProbeOfCurrentBuilding(),
                    transformReferenceoOfTutorialHand,
                    Vector3.zero,
                    0,
                    delegate {
                        if (!m_IsUserDrawing)
                        {
                            animatorReferenceForTutorialHand.SetTrigger("APPEAR");
                        }
                    },
                    delegate {
                        animatorReferenceForTutorialHand.SetTrigger("DISAPPEAR");
                    }
                );
        }
    }

    public void StopTutorial() {

        if (!useLevelLimiter || levelManagerReference.GetCurrentLevel() < levelLimit)
        {
            m_IsTutorialHandLookRotationControllerRunning = false;

            GlobalTouchController.Instance.OnTouchDown  -= OnTouchDown;
            GlobalTouchController.Instance.OnTouchUp    -= OnTouchUp;
        }
    }

    #endregion

}
