﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Gameplay;
using com.faithstudio.SDK;

//For Furthre Use
//#region Public Variables

//#endregion

//#region Private Variables

//#endregion

//#region Mono Behaviour

//#endregion

//#region Configuretion

//#endregion

//#region Public Callback

//#endregion

public class GameplayController : MonoBehaviour
{

    #region Public Variables

    public static GameplayController Instance;

    [Header("Configuretion      :   CameraView (Drawing")]
    public bool                             useBuildingCameraPositionOfDrawing;
    public Vector3                          cameraPositionOffsetForDrawing;
    public Vector3                          cameraFocusOffsetForDrawing;

    [Space(10.0f)]
    [Header("Configuretion      :   CameraView (Building Complete)")]
    public bool                             useBuildingCameraPositionOfCompleting;
    public Vector3                          cameraPositionOffsetForCompletingBuilding;
    public Vector3                          cameraFocusOffsetForCompletingBuilding;

    [Space(10.0f)]
    [Header("Configuretion      :   Gameplay")]
    [Range(0f,5f)]
    public float                            delayForTransationOnCompletingCircuit =2;
    [Range(0f,5f)]
    public float                            delayForTransationOnCompletingLevel = 3;

    [Space(5.0f)]
    [Header("External Reference")]
    public LayerMask                        layerMaskForConnectionProbe;
    public Transform                        cameraTransformReference;
    public ParticleSystem                   celebretionParticleForLevelEnd;

    [Space(5.0f)]
    public CameraMovementController         cameraMovementControllerReference;

    [Space(5.0f)]
    public LevelManager                     levelManagerReference;
    public LineDrawingController            lineDrawingControllerReference;
    public LineDrawingAnimationController   lineDrawingAnimationControllerReference;
    public GuidedPathGenerator              guidedPathGeneratorReference;
    public CircuitCompletationController    circuitCompletationControllerReference;
    public IlluminationController           illuminationControllerReference;
    public TutorialController               tutorialControllerReference;

    #endregion

    #region Private Variables

    private bool m_IsGameRunning;

    private Ray         m_Ray;
    private RaycastHit  m_RayCastHit;

    private BuildingAttribute m_SelectedBuildingAttribute;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
        }
        else
        {

            Destroy(gameObject);
        }
    }

    private void Start()
    {
        cameraMovementControllerReference.FocusCamera(
                levelManagerReference.GetTransformReferenceOfCompletedLevel(),
                Vector3.zero,
                Vector3.up * 15
            );
    }

    #endregion

    #region Configuretion

    private void OnRayCastOnConnectionProbe(Vector3 t_RayCastTargetInWorldSpace)
    {

        m_Ray = new Ray(
                cameraTransformReference.position,
                Vector3.Normalize(t_RayCastTargetInWorldSpace - cameraTransformReference.position)
            );
        if (Physics.Raycast(m_Ray, out m_RayCastHit, 240f, layerMaskForConnectionProbe))
        {

            ConnectionProbeOfBuilding t_ConnectionProbeOfBuildingReferece = m_RayCastHit.collider.GetComponent<ConnectionProbeOfBuilding>();
            if (t_ConnectionProbeOfBuildingReferece != null)
                t_ConnectionProbeOfBuildingReferece.OnRayCastedOnConnectionProbe();
        }
    }

    private IEnumerator ControllerForPreProcess() {

        WaitUntil t_WaitUntilTheGameEnd = new WaitUntil(() =>
        {
            if (m_IsGameRunning)
                return false;
            else
                return true;
        });

        GlobalTouchController.Instance.EnableTouchController();
        
        int t_CurrentLevel              = levelManagerReference.GetCurrentLevel();
        int t_NumberOfBuildingReference = levelManagerReference.levelInfos[t_CurrentLevel].buildingAttributesReferecen.Length;

        Transform t_NewCameraPivot = new GameObject().transform;

        yield return new WaitForSeconds(0.1f);

#if UNITY_IOS

        HapticFeedbackController.Instance.EnableHapticFeedback();
        FacebookAnalyticsManager.Instance.FBALevelStart(t_CurrentLevel);

#endif

        for (int buildingIndex = 0; buildingIndex < t_NumberOfBuildingReference; buildingIndex++) {

            bool t_WaitForDraw = true;

            m_SelectedBuildingAttribute = levelManagerReference.levelInfos[t_CurrentLevel].buildingAttributesReferecen[buildingIndex];

            if (useBuildingCameraPositionOfDrawing)
            {
                cameraMovementControllerReference.FocusCameraWithOrigin(
                    m_SelectedBuildingAttribute.cameraPositionReferenceForDrawing,
                    new List<Transform>() { m_SelectedBuildingAttribute.transform },
                    Vector3.zero,
                    Vector3.zero
                );
            }
            else {
                t_NewCameraPivot.position = m_SelectedBuildingAttribute.transform.position + cameraPositionOffsetForDrawing;
                cameraMovementControllerReference.FocusCameraWithOrigin(
                    t_NewCameraPivot,
                    new List<Transform>() { m_SelectedBuildingAttribute.transform },
                    cameraFocusOffsetForDrawing
                );
            }

            int t_IlluminationColorIndex = illuminationControllerReference.GetRandomIlluminationIndex();
            levelManagerReference.levelInfos[t_CurrentLevel].SetLightColorForBuildng(buildingIndex, t_IlluminationColorIndex);

            m_SelectedBuildingAttribute.PreProcess(delegate {

                t_WaitForDraw = false;
            },
            t_IlluminationColorIndex);
            guidedPathGeneratorReference.ShowGuidedPath(
                m_SelectedBuildingAttribute.guidedLine);
            lineDrawingControllerReference.PreProcess(m_SelectedBuildingAttribute.transform.position);
            circuitCompletationControllerReference.PreProcess(m_SelectedBuildingAttribute);

            lineDrawingControllerReference.OnPassingDataOfRayCastPoint += OnRayCastOnConnectionProbe;
            lineDrawingControllerReference.OnPassingDataOfRayCastPoint += guidedPathGeneratorReference.CastRayOnGuidedProbeThroughWorldSpace;

            WaitUntil t_WaitUntilTheDrawIsComplete = new WaitUntil(() =>
            {
                if (t_WaitForDraw)
                    return false;
                else
                    return true;
            });

            circuitCompletationControllerReference.EnableTouchInputControl();

            yield return new WaitForSeconds(1f);

            tutorialControllerReference.ShowTutorial(m_SelectedBuildingAttribute.transform.position);

            yield return t_WaitUntilTheDrawIsComplete;
            tutorialControllerReference.StopTutorial();
            circuitCompletationControllerReference.DisableTouchInputControl();
            guidedPathGeneratorReference.HideGuidedPath();
            

            //Converting Transform -> Vector3
            List<Transform> t_ListOfGuidedLinePosition  = m_SelectedBuildingAttribute.guidedLine;
            int t_NumberOfPivotPoints                   = t_ListOfGuidedLinePosition.Count;
            List<Vector3> t_PivotPoints = new List<Vector3>();
            for (int i = 0; i < t_NumberOfPivotPoints; i++) {

                t_PivotPoints.Add(t_ListOfGuidedLinePosition[i].position);
            }

            bool t_WaitForDrawAnimation = true;

            
            lineDrawingControllerReference.FadingOutLineRenderer(
                0f,
                delegate {

                    lineDrawingControllerReference.PostProcess();
                    lineDrawingControllerReference.OnPassingDataOfRayCastPoint -= OnRayCastOnConnectionProbe;
                    lineDrawingControllerReference.OnPassingDataOfRayCastPoint -= guidedPathGeneratorReference.CastRayOnGuidedProbeThroughWorldSpace;
                    lineDrawingControllerReference.ResetLineRenderer();
                    
                }
                );

            //Wait for DrawnLine ot Fade Out
            yield return new WaitForSeconds(1f);

            

            lineDrawingAnimationControllerReference.ShowLineDrawingAnimation(
                    t_PivotPoints,
                    Vector3.up * 0.1f,
                    0,
                    0,
                    0,
                    null,
                    null,
                    delegate
                    {
                        t_WaitForDrawAnimation = false;
                    }
                );

            WaitUntil t_WaitUntilTheDrawAnimationIsComplete = new WaitUntil(() =>
            {
                if (t_WaitForDrawAnimation)
                    return false;
                else
                    return true;
            });

            yield return t_WaitUntilTheDrawAnimationIsComplete;

            if (useBuildingCameraPositionOfCompleting)
            {
                t_NewCameraPivot.position = m_SelectedBuildingAttribute.transform.position + cameraPositionOffsetForCompletingBuilding;
                cameraMovementControllerReference.FocusCameraWithOrigin(
                        m_SelectedBuildingAttribute.cameraPositionReferenceForCompleting,
                        new List<Transform>() { m_SelectedBuildingAttribute.transform },
                        Vector3.zero,
                        Vector3.zero
                    );
            }
            else { 
                cameraMovementControllerReference.FocusCameraWithOrigin(
                        t_NewCameraPivot,
                        new List<Transform>() { m_SelectedBuildingAttribute.transform },
                        cameraFocusOffsetForCompletingBuilding
                    );
            }

            m_SelectedBuildingAttribute.AppearBuilding();
            m_SelectedBuildingAttribute.ThrowCelebretionParticle();
            m_SelectedBuildingAttribute.AppearSurroundingEnvironment(true);

            yield return new WaitForSeconds(delayForTransationOnCompletingCircuit);

        }

        Destroy(t_NewCameraPivot.gameObject);

#if UNITY_IOS

        FacebookAnalyticsManager.Instance.FBALevelComplete(t_CurrentLevel);
        HapticFeedbackController.Instance.DisableHapticFeedback();

#endif

        StartCoroutine(ControllerForPostProcess());
    }

    private IEnumerator ControllerForPostProcess() {

        circuitCompletationControllerReference.PostProcess();
        GlobalTouchController.Instance.DisableTouchController();

        int t_CurrentLevel = levelManagerReference.GetCurrentLevel();
        int t_NumberOfBuildingReference = levelManagerReference.levelInfos[t_CurrentLevel].buildingAttributesReferecen.Length;

        List<Transform> t_ListOfCameraTarget = new List<Transform>();
        for (int buildingIndex = 0; buildingIndex < t_NumberOfBuildingReference; buildingIndex++) {

            t_ListOfCameraTarget.Add(levelManagerReference.levelInfos[t_CurrentLevel].buildingAttributesReferecen[buildingIndex].transform);
            levelManagerReference.levelInfos[t_CurrentLevel].buildingAttributesReferecen[buildingIndex].ThrowCelebretionParticle();
        }

        cameraMovementControllerReference.FocusCamera(
                levelManagerReference.GetTransformReferenceOfCompletedLevel(),
                Vector3.zero,
                Vector3.up * 15
            );

        celebretionParticleForLevelEnd.Play();

        int t_MaxLevel = levelManagerReference.GetMaxLevel();
        if (t_CurrentLevel == (t_MaxLevel - 1))
        {

            for (int i = 0; i < t_CurrentLevel; i++)
            {

                int t_NumberOfBuilding = levelManagerReference.levelInfos[i].buildingAttributesReferecen.Length;
                for (int j = 0; j < t_NumberOfBuilding; j++)
                {

                    if (Random.Range(0f, 1f) <= 0.5f)
                    {

                        levelManagerReference.levelInfos[i].buildingAttributesReferecen[j].ThrowCelebretionParticle();
                    }
                }
            }

            yield return new WaitForSeconds(delayForTransationOnCompletingLevel * 2f);
        }
        else {

            yield return new WaitForSeconds(delayForTransationOnCompletingLevel);
        }

        

        celebretionParticleForLevelEnd.Stop();

        for (int buildingIndex = 0; buildingIndex < t_NumberOfBuildingReference; buildingIndex++)
        {
            levelManagerReference.levelInfos[t_CurrentLevel].buildingAttributesReferecen[buildingIndex].PostProcess();
        }


        levelManagerReference.IncreaseLevel();
        UIStateController.Instance.AppearMainMenu();

        yield return null;

        StopCoroutine(ControllerForPostProcess());

        m_IsGameRunning = false;
    }

    #endregion

    #region Public Callback

    public void PreProcess() {

        if (!m_IsGameRunning) {

            m_IsGameRunning = true;
            StartCoroutine(ControllerForPreProcess());
        }
    }

    public List<Transform> GetListOfTransformReferenceOfGuidedProbeOfCurrentBuilding() {

        return m_SelectedBuildingAttribute.guidedLine;
    }

    #endregion
}
