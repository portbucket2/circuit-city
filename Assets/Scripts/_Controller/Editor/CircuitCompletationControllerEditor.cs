﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CircuitCompletationController))]
public class CircuitCompletationControllerEditor : Editor
{
    private CircuitCompletationController Reference;

    private void OnEnable()
    {
        Reference = (CircuitCompletationController)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

}
