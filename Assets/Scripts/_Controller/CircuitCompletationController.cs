﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using com.faithstudio.Math;
using com.faithstudio.Gameplay;

public class CircuitCompletationController : MonoBehaviour
{

    #region Custom Variables

    public enum CircuitCompleteRule
    {
        CompleteThroughIllumination,
        CompleteThroughConnection,
        CompleteThroughFollowingGuidedPath
    }

    [System.Serializable]
    public class LineMap
    {
        public int      lineIndex;
        public List<int> listOfConnectedLineIndex;

        public LineMap(int lineIndex) {

            this.lineIndex              = lineIndex;
            listOfConnectedLineIndex    = new List<int>();
        }

        public bool IsLineAlreadyConnected(int t_LineIndex) {

            if (listOfConnectedLineIndex.Contains(t_LineIndex))
                return true;

            return false;
        }

        public void AddToLineIndex(int t_LineIndex) {

            if (!IsLineAlreadyConnected(t_LineIndex)) {

                listOfConnectedLineIndex.Add(t_LineIndex);
            }
        }
    }

    [System.Serializable]
    public class ConnectionProbeInfo
    {

        public ConnectionProbeOfBuilding connectionProbeReference;

        public List<int> m_ListOfLineRendererConnectedWithProbe;

        public ConnectionProbeInfo() {

            m_ListOfLineRendererConnectedWithProbe = new List<int>();
        }    
    }

    #endregion

    #region Public Variables

    [Header("Configuretion  :   Rules of Circuit Complete")]
    public CircuitCompleteRule circuitCompleteRule = CircuitCompleteRule.CompleteThroughIllumination;
    [Range(0f,1f)]
    public float boundaryOfCompletingCircuit;

    [Space(10.0f)]
    [Header("Configuretion")]
    public bool isCheckForIlluminationOnRunTime;
    [Range(0.1f,1f)]
    public float delayOnCycle = 0.1f;

    [Space(5.0f)]
    [Range(0f,1f)]
    public float samplingDistance = 0.1f;

    [Space(5.0f)]
    [Header("Referebce  :   External")]
    public LineDrawingController    lineDrawingControllerReference;
    public GuidedPathGenerator      guidedPathGenerator;

    #endregion

    #region Private Variables

    private bool m_IsTouchInputControlEnabled;
    private bool m_IsControllerForCheckingIlluminationRunning;

    private int m_NumberOfConnectionProbe;

    private MathFunction                m_MathFunctionReference;
    private BuildingAttribute           m_BuildingAttributeReference;

    //[SerializeField]
    private List<LineMap>                m_ListOfLineMap;
    //[SerializeField]
    private List<int>                   m_ListOfLineAlreadyIntersectedWithProbe;
    //[SerializeField]
    private List<ConnectionProbeInfo>   m_ListOfConnectionProbeOfTheBuilding;

    #endregion

    #region Mono Behaviour

    private void Start()
    {
        m_MathFunctionReference = MathFunction.Instance;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForCheckingIllumination() {

        WaitForSeconds t_CycleDelay = new WaitForSeconds(delayOnCycle);

        while (m_IsControllerForCheckingIlluminationRunning) {


            if (m_IsTouchInputControlEnabled) {

                CheckForLineIntersection();
                CheckForLightIllumination();
            }
            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForCheckingIllumination());
    }

    private List<int> GetListOfConnectedLineWithProbe(ConnectionProbeOfBuilding t_ConnectionProbeOfTheBuilding) {

        int t_NumberOfConnectionProbeOfTheBuilidng = m_ListOfConnectionProbeOfTheBuilding.Count;
        for (int i = 0; i < t_NumberOfConnectionProbeOfTheBuilidng; i++) {

            if (m_ListOfConnectionProbeOfTheBuilding[i].connectionProbeReference == t_ConnectionProbeOfTheBuilding) {

                return m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe;
            }
        }

        return null;
    }

    private void OnTouchDown(Vector3 t_TouchPosition) {

        m_ListOfLineMap.Add(new LineMap(lineDrawingControllerReference.GetNumberOfLineInTheScene() - 1));

        if (isCheckForIlluminationOnRunTime && !m_IsControllerForCheckingIlluminationRunning) {

            m_IsControllerForCheckingIlluminationRunning = true;
            StartCoroutine(ControllerForCheckingIllumination());
        }
    }

    private void OnTouchUp(Vector3 t_TouchPosition)
    {
        if (m_IsTouchInputControlEnabled) {

            CheckForLineIntersection();
            CheckForLightIllumination();
        }

        if(isCheckForIlluminationOnRunTime)
            m_IsControllerForCheckingIlluminationRunning = false;
    }

    private void CheckInterlatedConnectionOfLineThroughProbe() {

        bool t_IsStaysAtLoop = true;
        while (t_IsStaysAtLoop)
        {

            t_IsStaysAtLoop = false;
            int t_NumberOfConnection = m_ListOfConnectionProbeOfTheBuilding.Count;
            for (int i = 0; i < t_NumberOfConnection; i++)
            {

                int t_NumberOfLineConnectedWithProbe = m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe.Count;

                for (int j = 0; j < t_NumberOfLineConnectedWithProbe; j++)
                {
                    int t_LineMapIndex = m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe[j];
                    
                    if (t_LineMapIndex >= 0 && t_LineMapIndex < m_ListOfLineMap.Count)
                    {

                        int t_NumberOfLineRendererInterConnected = m_ListOfLineMap[t_LineMapIndex].listOfConnectedLineIndex.Count;
                        for (int k = 0; k < t_NumberOfLineRendererInterConnected; k++)
                        {

                            int t_InterConnectedLineIndex = m_ListOfLineMap[m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe[j]].listOfConnectedLineIndex[k];
                            if (!m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe.Contains(t_InterConnectedLineIndex))
                            {
                                m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe.Add(t_InterConnectedLineIndex);
                                t_IsStaysAtLoop = true;
                            }
                        }
                    }
                }
            }
        }
    }

    private bool CheckForLightIllumination() {

        // t_NumberOfLight = 4
        int t_NumberOfLightIlluminated = 0;
        int t_NumberOfLight = m_BuildingAttributeReference.newConnectionMatrix.Length;
        for (int i = 0; i < t_NumberOfLight; i++) {

            int t_LightIlluminationChecker = 0;

            // i =0 : t_NumberOfConnectionProbeRequiredForLight = 2
            int t_NumberOfConnectionProbeRequiredForLight = m_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit.Length;
            for (int j = 0; j < t_NumberOfConnectionProbeRequiredForLight - 1; j++) {

                List<int> t_ConnectionReference1 = new List<int>(GetListOfConnectedLineWithProbe(m_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j]));
                List<int> t_ConnectionReference2 = new List<int>(GetListOfConnectedLineWithProbe(m_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j + 1]));

                int t_NumberOfCheck = t_ConnectionReference1.Count;
                for (int k = 0; k < t_NumberOfCheck; k++)
                {

                    if (t_ConnectionReference2.Contains(t_ConnectionReference1[k]))
                    {
                        //Debug.Log(m_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j].name + " |IsConnected| " + m_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j + 1].name + " |Through Line| " + t_ConnectionReference1[k]);
                        t_LightIlluminationChecker++;
                        break;
                    }
                }
            }

            if (t_NumberOfConnectionProbeRequiredForLight == (t_LightIlluminationChecker + 1)) {

                m_BuildingAttributeReference.IlluminateLight(i);
                t_NumberOfLightIlluminated++;
            }

        }

        if (t_NumberOfLightIlluminated == t_NumberOfLight)
            return true;

        return false;
    }

    private bool IsCircuitFullConnected() {

        int t_NumberOfVisitedNode   = m_ListOfLineAlreadyIntersectedWithProbe.Count;
        int t_NumberOfConnection    = m_ListOfConnectionProbeOfTheBuilding.Count;
        for (int i = 0; i < t_NumberOfConnection - 1; i++) {

            for (int j = 0; j < t_NumberOfVisitedNode; j++) {

                if (!m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe.Contains(m_ListOfLineAlreadyIntersectedWithProbe[j])) {

                    return false;
                }
            }
        }

        return true;
    }

    #endregion

    #region Public Callback

    public void PreProcess(BuildingAttribute t_BuildingAttributeReference)
    {
        m_BuildingAttributeReference    = t_BuildingAttributeReference;

        List<ConnectionProbeOfBuilding> t_ListOfConnectionProbeOfTheBuilding = new List<ConnectionProbeOfBuilding>();
        int t_NumberOfConnectionMatrix  = t_BuildingAttributeReference.newConnectionMatrix.Length;
        for (int i = 0; i < t_NumberOfConnectionMatrix; i++) {

            int t_NumberOfConnectionProbe = t_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit.Length;
            for (int j = 0; j < t_NumberOfConnectionProbe; j++) {

                if (!t_ListOfConnectionProbeOfTheBuilding.Contains(t_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j])) {

                    t_ListOfConnectionProbeOfTheBuilding.Add(t_BuildingAttributeReference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j]);
                }
            }
        }

        m_NumberOfConnectionProbe       = t_ListOfConnectionProbeOfTheBuilding.Count;
        m_ListOfConnectionProbeOfTheBuilding  = new List<ConnectionProbeInfo>();

        for (int i = 0; i < m_NumberOfConnectionProbe; i++) {

            m_ListOfConnectionProbeOfTheBuilding.Add(new ConnectionProbeInfo()
            {
                connectionProbeReference = t_ListOfConnectionProbeOfTheBuilding[i]
            });

            t_ListOfConnectionProbeOfTheBuilding[i].OnConnectionProbeTouch += OnConnectionProbeTouched;
        }

        m_ListOfLineAlreadyIntersectedWithProbe = new List<int>();
        m_ListOfLineMap                         = new List<LineMap>();


        
        GlobalTouchController.Instance.OnTouchDown  += OnTouchDown;
        GlobalTouchController.Instance.OnTouchUp    += OnTouchUp;
        m_IsTouchInputControlEnabled = true;
    }

    public void PostProcess() {

        m_IsControllerForCheckingIlluminationRunning = false;
        m_IsTouchInputControlEnabled = false;
        GlobalTouchController.Instance.OnTouchDown  -= OnTouchDown;
        GlobalTouchController.Instance.OnTouchUp    -= OnTouchUp;
    }

    public void EnableTouchInputControl() {

        m_IsTouchInputControlEnabled = true;
    }

    public void DisableTouchInputControl()
    {
        m_IsTouchInputControlEnabled = false;
    }

    public void OnConnectionProbeTouched(ConnectionProbeOfBuilding t_ConnectionProbe) 
    {
        
        for (int i = 0; i < m_NumberOfConnectionProbe; i++) {

            if (m_ListOfConnectionProbeOfTheBuilding[i].connectionProbeReference == t_ConnectionProbe) {

                int t_NumberOfLines = lineDrawingControllerReference.GetNumberOfLineInTheScene() - 1;
                if (!m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe.Contains(t_NumberOfLines)) {

                    m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe.Add(t_NumberOfLines);

                    //Remaping : For Being Connected With The Same Probe
                    int t_NumberOfLineRendererConnectedWithProbe = m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe.Count;
                    for (int j = 0; j < t_NumberOfLineRendererConnectedWithProbe - 1; j++) {

                        for (int k = j + 1; k < t_NumberOfLineRendererConnectedWithProbe; k++) {

                            if (j != k) {

                                m_ListOfLineMap[m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe[j]].AddToLineIndex(m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe[k]);
                                m_ListOfLineMap[m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe[k]].AddToLineIndex(m_ListOfConnectionProbeOfTheBuilding[i].m_ListOfLineRendererConnectedWithProbe[j]);
                            }
                        }
                    }


                    if (!m_ListOfLineAlreadyIntersectedWithProbe.Contains(t_NumberOfLines)) {

                        m_ListOfLineAlreadyIntersectedWithProbe.Add(t_NumberOfLines);
                        
                    }
                }
            }
        }
    }

    public void CheckForLineIntersection()
    {

        int t_NumberOfLineDrawn = lineDrawingControllerReference.GetNumberOfLineInTheScene();
        
        for (int i = 0; i < t_NumberOfLineDrawn; i++)
        {

            Vector3[] t_PositionsOfThePointWhichIsNotConnected = lineDrawingControllerReference.GetPositionsOfLineRenderer(i);
            for (int j = 0; j < t_NumberOfLineDrawn; j++)
            {

                if (i != j)
                {

                    Vector3[] t_PositionOfPointsConnected = lineDrawingControllerReference.GetPositionsOfLineRenderer(j);

                    int t_NumberOfPointOnNotConnectedLine = t_PositionsOfThePointWhichIsNotConnected.Length;
                    int t_NumberOfPointOnConnectedLine = t_PositionOfPointsConnected.Length;
                    int t_SamplingDistanceForNotConnectedLine = 1;//= Mathf.FloorToInt(t_NumberOfPointOnNotConnectedLine * samplingDistance);
                    int t_SamplingDistanceForConnectedLine = 1;// Mathf.FloorToInt(t_NumberOfPointOnConnectedLine * samplingDistance);

                    //Debug.Log(
                    //    "Line (" + i + ") | "
                    //    + t_SamplingDistanceForNotConnectedLine
                    //    + " | " + t_NumberOfPointOnNotConnectedLine
                    //    + " -> Line (" + j + ") | "
                    //    + t_SamplingDistanceForConnectedLine
                    //    + " | " + t_NumberOfPointOnConnectedLine);

                    bool t_ForceQuit = false;

                    for (int nonConnectedIndex = 0; nonConnectedIndex < t_NumberOfPointOnNotConnectedLine - 1; nonConnectedIndex++)
                    {

                        for (int connectedIndex = 0; connectedIndex < t_NumberOfPointOnConnectedLine - 1; connectedIndex++)
                        {

                            Vector2 t_Point1OfNotConnectedLine = new Vector2(
                                    t_PositionsOfThePointWhichIsNotConnected[nonConnectedIndex].x,
                                    t_PositionsOfThePointWhichIsNotConnected[nonConnectedIndex].z
                                );
                            Vector2 t_Point2OfNotConnectedLine = new Vector2(
                                    t_PositionsOfThePointWhichIsNotConnected[nonConnectedIndex + t_SamplingDistanceForNotConnectedLine].x,
                                    t_PositionsOfThePointWhichIsNotConnected[nonConnectedIndex + t_SamplingDistanceForNotConnectedLine].z
                                );

                            Vector2 t_Point1OfConnectedLine = new Vector2(
                                    t_PositionOfPointsConnected[connectedIndex].x,
                                    t_PositionOfPointsConnected[connectedIndex].z
                                );

                            Vector2 t_Point2OfConnectedLine = new Vector2(
                                    t_PositionOfPointsConnected[connectedIndex + t_SamplingDistanceForConnectedLine].x,
                                    t_PositionOfPointsConnected[connectedIndex + t_SamplingDistanceForConnectedLine].z
                                );

                            if (m_MathFunctionReference.IsTwoLineIntersected(
                                    t_Point1OfNotConnectedLine,
                                    t_Point2OfNotConnectedLine,
                                    t_Point1OfConnectedLine,
                                    t_Point2OfConnectedLine))
                            {

                                m_ListOfLineMap[i].AddToLineIndex(j);
                                m_ListOfLineMap[j].AddToLineIndex(i);


                                //Debug.Log(
                                //    "Intersected ::: Line (" + i + ") | "
                                //    + t_Point1OfNotConnectedLine
                                //    + " : " + t_Point2OfNotConnectedLine
                                //    + " -> Line (" + j + ") | "
                                //    + t_Point1OfConnectedLine
                                //    + " : "
                                //    + t_Point2OfConnectedLine);

                                t_ForceQuit = true;
                                break;
                            }

                        }

                        if (t_ForceQuit)
                            break;

                    }
                }
            }
        }

        //Updating List
        CheckInterlatedConnectionOfLineThroughProbe();
        
        if (IsGameComplete())
        {
            m_BuildingAttributeReference.OnCircuitComplete?.Invoke();
        }
    }

    public bool IsGameComplete()
    {
        switch (circuitCompleteRule)
        {

            case CircuitCompleteRule.CompleteThroughConnection:
                if (CheckForLightIllumination() && IsCircuitFullConnected())
                    return true;
                else
                    return false;
            case CircuitCompleteRule.CompleteThroughIllumination:
                if (CheckForLightIllumination())
                    return true;
                else
                    return false;
            case CircuitCompleteRule.CompleteThroughFollowingGuidedPath:
                if (guidedPathGenerator.GetProgressionOfFollowingGuidedPath() >= boundaryOfCompletingCircuit && CheckForLightIllumination())
                    return true;
                else
                    return false;
            default:
                return false;

        }

    }

    #endregion
}
