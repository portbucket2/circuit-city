﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class CameraMovementController : MonoBehaviour
{

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(
                m_NewCameraBounds.center,
                m_NewCameraBounds.size
            );

        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(
                m_NewCameraBounds.center,
                m_NewCameraBounds.size * cameraBoundExtend
            );
    }

    #region Public Variables

#if UNITY_EDITOR

    [Header("Configuretion  :   Editor")]
    public List<Transform> editorCameraTarget;

#endif

    [Space(10.0f)]
    [Header("Configuretion  :   Core Valus & References")]
    [Range(0f, 1f)]
    public float        defaultMaxFocusingValue;
    public Camera       cameraReference;

    [Space(10.0f)]
    public Transform    cameraContainerTransformReference;
    public Transform    cameraTransformReference;
    [Range(0f, 1f)]
    public float        smoothTimeForForwardVelocity;
    [Range(0f, 1f)]
    public float        angulerVelocity;

    [Space(10.0f)]
    [Header("Configuretion  :   CameraViewOnTargets")]
    [Range(1f,5f)]
    public float cameraBoundExtend = 1;
    [Range(0f,1f)]
    public float cameraPositionOnX;
    [Range(0f,1f)]
    public float cameraPositionOnY;
    [Range(0f, 1f)]
    public float cameraPositionOnZ;
    public Vector3 defaultCameraFocusOffset;
    public Vector3 defaultCameraPositionOffset;

    #endregion

    #region Private Variables

    private bool            m_IsCameraOrthographic;
    private bool            m_IsCameraMovementEnabled;

    private int             m_NumberOfCameraFocuses;

    private float           m_InitialCameraFieldOfView;
    private float           m_InitialOrthographicSize;

    private float           m_CameraReactingSpeed;
    private float           m_MinZoom;
    private float           m_MaxZoom;
    private float           m_TargetedZoom;
    private float           m_ModifiedZoom;

    private Bounds          m_NewCameraBounds;

    private Transform       m_TransformReferenceOfCameraOrigin;
    private List<Transform> m_ListOfCameraFocuses;

    private Vector3         m_CameraMovementVelocity;
    private Vector3         m_CameraFocusOffset;
    private Vector3         m_CameraPositionOffset;

    private Vector3         m_FocusPosition;
    private Vector3         m_FocusedEulerAngle;

    private Vector3         m_CurrentPositionOfTheCamera;
    private Vector3         m_NewOriginPositionOfCamera;

    private Vector3         m_ModifiedPosition;
    private Quaternion      m_ModifiedRotation;

    private UnityAction OnCameraReachedTargetedPosition;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {

        m_InitialCameraFieldOfView  = cameraReference.fieldOfView;
        m_InitialOrthographicSize   = cameraReference.orthographicSize;

        CalculateFOVAndOrthographicSize(defaultMaxFocusingValue);

    }

    private void Start()
    {
        
    }

    private void LateUpdate()
    {
        if (m_IsCameraMovementEnabled) {

            CalculateCameraBound();
            MoveCamera();
            RotateCamera();
            ZoomCamera();
            
        }
    }

    #endregion

    #region Configuretion

    private void CalculateFOVAndOrthographicSize(float t_MaxFocusingValue)
    {

        if (cameraReference.orthographic)
        {
            m_IsCameraOrthographic = true;

            m_MinZoom = m_InitialOrthographicSize;
            m_MaxZoom = m_MinZoom * (1f - t_MaxFocusingValue);
        }
        else
        {
            m_IsCameraOrthographic = false;

            m_MinZoom = m_InitialCameraFieldOfView;
            m_MaxZoom = m_MinZoom * (1f - t_MaxFocusingValue);
        }
    }

    private void CalculateCameraBound() {

        m_NewCameraBounds = new Bounds(m_ListOfCameraFocuses[0].position, Vector3.zero);
        for (int i = 0; i < m_NumberOfCameraFocuses; i++)
        {
            m_NewCameraBounds.Encapsulate(m_ListOfCameraFocuses[i].position);
        }

        m_FocusPosition     = m_NewCameraBounds.center + m_CameraFocusOffset;
        m_FocusedEulerAngle = Quaternion.LookRotation(m_NewCameraBounds.center - cameraTransformReference.position).eulerAngles;

    }

    private void MoveCamera() {

        m_CurrentPositionOfTheCamera        = cameraContainerTransformReference.position;

        if(m_TransformReferenceOfCameraOrigin != null)
            m_NewOriginPositionOfCamera         = m_TransformReferenceOfCameraOrigin.position;

        m_ModifiedPosition = Vector3.SmoothDamp(
                m_CurrentPositionOfTheCamera,
                m_NewOriginPositionOfCamera + m_CameraPositionOffset,
                ref m_CameraMovementVelocity,
                smoothTimeForForwardVelocity
            );
        cameraContainerTransformReference.position = m_ModifiedPosition;

        //Debug.Log(Vector3.Distance(m_ModifiedPosition, m_NewOriginPositionOfCamera) + " : " + Vector3.Distance(cameraTransformReference.eulerAngles, m_FocusedEulerAngle));
        if (Vector3.Distance(m_ModifiedPosition, m_NewOriginPositionOfCamera) <= 0.1f && Vector3.Distance(cameraTransformReference.eulerAngles, m_FocusedEulerAngle) <= 0.1)
        {
            Debug.Log("Camera Movement Disabled");
            m_IsCameraMovementEnabled = false;
            OnCameraReachedTargetedPosition?.Invoke();
        }
    }

    private void RotateCamera() {

        m_ModifiedRotation = Quaternion.Slerp(
                            cameraTransformReference.rotation,
                            Quaternion.LookRotation(m_FocusPosition - cameraTransformReference.position),
                            m_CameraReactingSpeed
                        );
        cameraTransformReference.rotation = m_ModifiedRotation;
    }

    private void ZoomCamera() {

        if (m_ListOfCameraFocuses != null)
        {


            m_TargetedZoom = Mathf.Lerp(
                m_MaxZoom,
                m_MinZoom,
                (((m_NewCameraBounds.size.x + m_NewCameraBounds.size.y + m_NewCameraBounds.size.z) / 3.0f) / m_MinZoom));

            if (m_IsCameraOrthographic)
            {
                m_ModifiedZoom = Mathf.Lerp(
                        cameraReference.orthographicSize,
                        m_TargetedZoom,
                        Time.deltaTime
                    );

                cameraReference.orthographicSize = m_ModifiedZoom;
            }
            else
            {

                m_ModifiedZoom = Mathf.Lerp(
                        cameraReference.fieldOfView,
                        m_TargetedZoom,
                        Time.deltaTime
                    );

                cameraReference.fieldOfView = m_ModifiedZoom;
            }
        }
    }

    private void ConfigureCamera(
        List<Transform> t_ListOfCameraFocuses,
        Vector3         t_CameraFocusOffset = new Vector3(),
        Vector3         t_CameraPositionOffset = new Vector3(),
        float           t_CameraMovementSpeed = 0,
        UnityAction     OnCameraReachedTargetedPosition = null,
        Transform       t_CameraOriginWhileTarget = null) {

        m_TransformReferenceOfCameraOrigin = t_CameraOriginWhileTarget;

        m_NumberOfCameraFocuses = t_ListOfCameraFocuses.Count;
        m_ListOfCameraFocuses = t_ListOfCameraFocuses;

        
        if (t_CameraFocusOffset == Vector3.zero)
            m_CameraFocusOffset = defaultCameraFocusOffset;
        else
            m_CameraFocusOffset = t_CameraFocusOffset;

        if (t_CameraPositionOffset == Vector3.zero)
            m_CameraPositionOffset = defaultCameraPositionOffset;
        else
            m_CameraPositionOffset = t_CameraPositionOffset;

        if (t_CameraMovementSpeed == 0)
            m_CameraReactingSpeed = angulerVelocity;
        else
        {
            m_CameraReactingSpeed = Mathf.SmoothStep(0f, 1f, t_CameraMovementSpeed);
        }

        this.OnCameraReachedTargetedPosition = OnCameraReachedTargetedPosition;

        //Calculating Bound
        CalculateCameraBound();
        Vector3 t_NewBoundSize = m_NewCameraBounds.size;
        float t_MaxLengthOfSize = Mathf.Max(t_NewBoundSize.x, t_NewBoundSize.z);

        t_NewBoundSize = new Vector3(
                t_MaxLengthOfSize,
                t_NewBoundSize.y,
                t_MaxLengthOfSize
            ) * cameraBoundExtend;

        //t_NewBoundSize = new Vector3(
        //        t_NewBoundSize.x <= 0.1f ? t_MaxLengthOfSize : t_NewBoundSize.x,
        //        t_NewBoundSize.y <= 0.1f ? t_MaxLengthOfSize : t_NewBoundSize.y,
        //        t_NewBoundSize.z <= 0.1f ? t_MaxLengthOfSize : t_NewBoundSize.z
        //    ) * cameraBoundExtend;

        m_NewOriginPositionOfCamera =
            m_NewCameraBounds.center
            + new Vector3(
                Mathf.SmoothStep(-(t_NewBoundSize.x / 2.0f), t_NewBoundSize.x / 2.0f, cameraPositionOnX),
                Mathf.SmoothStep(-(t_NewBoundSize.y / 2.0f), t_NewBoundSize.y / 2.0f, cameraPositionOnY),
                Mathf.SmoothStep(-(t_NewBoundSize.z / 2.0f), t_NewBoundSize.z / 2.0f, cameraPositionOnZ)
            )
            + defaultCameraPositionOffset;

        m_IsCameraMovementEnabled = true;
    }

    #endregion

    #region Public Callback

    public void FocusCameraWithOrigin(
        Transform       t_CameraOriginWhileTarget,
        List<Transform> t_ListOfCameraFocuses,
        Vector3         t_CameraFocusOffset = new Vector3(),
        Vector3         t_CameraPositionOffset = new Vector3(),
        float           t_CameraMovementSpeed = 0,
        UnityAction OnCameraReachedTargetedPosition = null
        )
    {
        ConfigureCamera(
                t_ListOfCameraFocuses,
                t_CameraFocusOffset,
                t_CameraPositionOffset,
                t_CameraMovementSpeed,
                OnCameraReachedTargetedPosition,
                t_CameraOriginWhileTarget
            );
    }

    public void FocusCamera(
        List<Transform> t_ListOfCameraFocuses,
        Vector3         t_CameraFocusOffset = new Vector3(),
        Vector3         t_CameraPositionOffset = new Vector3(),
        float           t_CameraMovementSpeed = 0,
        UnityAction     OnCameraReachedTargetedPosition = null
        ) {

        ConfigureCamera(
                t_ListOfCameraFocuses,
                t_CameraFocusOffset,
                t_CameraPositionOffset,
                t_CameraMovementSpeed,
                OnCameraReachedTargetedPosition,
                null
            );
    }

    #endregion
}
