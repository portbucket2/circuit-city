﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Gameplay;

[RequireComponent(typeof(LineRenderer))]
public class LineDrawingController : MonoBehaviour
{

    #region Custom Variables

    private void OnValidate()
    {
        m_LineDrawingOffset = new Vector3(
                Screen.width * lineDrawingOffsetOnHorizontal,
                Screen.height * lineDrawingOffsetOnVertical,
                0f
            );
    }

    #endregion

    #region Public Variables

    public delegate void DelegateForPassidDataOfRayCastPoint(Vector3 t_RayCastPointInWorldSpace);

    [Header("Reference      :   External")]
    public Camera mainCameraReference;
    public LayerMask layerOfConnectionProb;

    [Space(5.0f)]
    [Header("Configuretion  :   LineDrawing")]
    public bool multiLineDrawingEnabled;
    [Range(0f,2f)]
    public float defaultDurationForFadingOut;
    [Range(0f,0.1f)]
    public float    validDistanceForInput;
    [Range(0f,1f)]
    public float    lineDrawingOffsetOnVertical;
    [Range(0f, 1f)]
    public float    lineDrawingOffsetOnHorizontal;

    [Space(5.0f)]
    [Header("Configuretion  :   LineDrawingVisual")]
    public Animator     pointerAnimatorReference;
    public Transform    pointerTransformReference;
    public Vector3      pointerOffset;

    [Space(5.0f)]
    [Header("Configuretion  :   LineDrawingVisual (ProgressBar)")]
    [Range(0f,1f)]
    public float        delayForDrawing = 0.25f;
    public Animator     delayBarAnimatorReference;
    public Transform    delayBarTransformReference;
    public Vector3      positionOffsetForDelayBar;
    public Image        delayBarFillerReference;

    [HideInInspector]
    public DelegateForPassidDataOfRayCastPoint OnPassingDataOfRayCastPoint;

    #endregion

    #region Private Variables

    private bool        m_IsLineDrawingControllerRunning;
    private bool        m_IsDelayForDrawingControllerRunning;
    private bool        m_IsPointerShowing;

    private int         m_NumberOfLineInTheScene;
    private int         m_NumberOfLinePosition;
    private float       m_AbsoluteDistanceForValidInput;

    private Vector3     m_LineDrawingOffset;
    private Vector3     m_DelayIndicatorPosition;
    private Vector3     m_PivotForLineDrawing;

    private Transform   m_CameraTransformReference;

    private LineRenderer m_OriginLineRendererReference;
    private LineRenderer m_CurrentLineRendererReference;
    private List<LineRenderer> m_ListOfLineRenderers;

    private Gradient m_LineRendererGradient;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_OriginLineRendererReference   = gameObject.GetComponent<LineRenderer>();
        m_LineRendererGradient          = m_OriginLineRendererReference.colorGradient;

        m_CameraTransformReference      = mainCameraReference.transform;
    }

    private void Start()
    {
        m_LineDrawingOffset = new Vector3(
                Screen.width * lineDrawingOffsetOnHorizontal,
                Screen.height * lineDrawingOffsetOnVertical,
                0f
            );
    }

    #endregion

    #region Configuretion

    private void OnTouchDown(Vector3 t_TouchPosition)
    {

        if (multiLineDrawingEnabled)
        {
            LineRenderer t_NewLineRenderer      = Instantiate(m_OriginLineRendererReference);
            Destroy(t_NewLineRenderer.GetComponent<LineDrawingController>());

            m_CurrentLineRendererReference      = t_NewLineRenderer;

            m_ListOfLineRenderers.Add(t_NewLineRenderer);
            m_NumberOfLinePosition = 0;
            m_NumberOfLineInTheScene++;
        }

        Ray t_Ray = mainCameraReference.ScreenPointToRay(t_TouchPosition);
        RaycastHit t_RayCastHit;
        if (Physics.Raycast(t_Ray, out t_RayCastHit, 100f, layerOfConnectionProb)) {

            Vector3 t_LocalScale    = t_RayCastHit.transform.localScale;
            float t_ScaleFactor     = (t_LocalScale.x + t_LocalScale.y + t_LocalScale.z) / 3.0f;
            m_AbsoluteDistanceForValidInput = t_ScaleFactor * validDistanceForInput;

            m_DelayIndicatorPosition = t_RayCastHit.point;
        }

        if (!m_IsDelayForDrawingControllerRunning) {

            m_IsDelayForDrawingControllerRunning = true;
            StartCoroutine(ControllerForDelayOnDrawing());
        }

    }

    private void OnTouch(Vector3 t_TouchPosition)
    {
        OnRayCast(t_TouchPosition);
    }

    private void OnTouchUp(Vector3 t_TouchPosition)
    {

        if (m_IsDelayForDrawingControllerRunning)
            m_IsDelayForDrawingControllerRunning = false;

        if (m_IsPointerShowing) {

            m_IsPointerShowing = false;
            pointerAnimatorReference.gameObject.SetActive(false);
            //pointerAnimatorReference.SetTrigger("DISAPPEAR");
        }
        
        OnRayCast(t_TouchPosition);
    }

    private void OnRayCast(Vector3 t_TouchPosition)
    {

        Ray t_Ray = mainCameraReference.ScreenPointToRay(t_TouchPosition + m_LineDrawingOffset);
        RaycastHit t_RayCastHit;
        if (Physics.Raycast(t_Ray, out t_RayCastHit, 100f, layerOfConnectionProb))
        {

            Vector3 t_ModifiedPosition = t_RayCastHit.point;

            //Tracking The Progress Bar
            m_DelayIndicatorPosition = t_ModifiedPosition;

            if (!m_IsDelayForDrawingControllerRunning)
            {
                OnChangingPointer(t_ModifiedPosition);
                OnPassingDataOfRayCastPoint?.Invoke(t_ModifiedPosition);

                if (m_NumberOfLinePosition > 0)
                {
                    if (Vector3.Distance(t_ModifiedPosition, m_CurrentLineRendererReference.GetPosition(m_NumberOfLinePosition - 1)) >= m_AbsoluteDistanceForValidInput)
                    {

                        m_CurrentLineRendererReference.positionCount = m_NumberOfLinePosition + 1;
                        m_CurrentLineRendererReference.SetPosition(m_NumberOfLinePosition, t_ModifiedPosition);
                        m_NumberOfLinePosition++;
                    }
                }
                else
                {

                    m_CurrentLineRendererReference.positionCount = m_NumberOfLinePosition + 1;
                    m_CurrentLineRendererReference.SetPosition(m_NumberOfLinePosition, t_ModifiedPosition);
                    m_NumberOfLinePosition++;
                }
            }
            
        }

        
    }

    private void OnChangingPointer(Vector3 t_PointerPosition) {

        Vector3 t_NewPointerPosition        = t_PointerPosition + pointerOffset;
        pointerTransformReference.position  = t_NewPointerPosition;
    }

    private IEnumerator ControllerForFadingOut(float t_DurationForFadingOut, UnityAction OnFadingOutEnd = null)
    {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_RemainingTimeForFadingOut = t_DurationForFadingOut;

        Gradient t_ColorGradientOfLineRenderer = m_LineRendererGradient;
        GradientAlphaKey[] t_AlphaKey = m_LineRendererGradient.alphaKeys;

        int t_NumberOfAlphaKey = t_ColorGradientOfLineRenderer.alphaKeys.Length - 1;
        for (int i = 0; i <= t_NumberOfAlphaKey; i++)
        {
            t_AlphaKey[i].alpha = (t_NumberOfAlphaKey - i) / ((float)t_NumberOfAlphaKey);
        }
        t_ColorGradientOfLineRenderer.SetKeys(t_ColorGradientOfLineRenderer.colorKeys, t_AlphaKey);

        Color t_ProgressedColor;

        int t_NumberOfLineRenderer = m_ListOfLineRenderers.Count;

        while (t_RemainingTimeForFadingOut > 0)
        {

            t_ProgressedColor = t_ColorGradientOfLineRenderer.Evaluate(1f - (t_RemainingTimeForFadingOut / t_DurationForFadingOut));

            if (multiLineDrawingEnabled)
            {
                for (int i = 0; i < t_NumberOfLineRenderer; i++)
                {

                    m_ListOfLineRenderers[i].startColor = t_ProgressedColor;
                    m_ListOfLineRenderers[i].endColor = t_ProgressedColor;
                }
            }
            else
            {

                m_CurrentLineRendererReference.startColor = t_ProgressedColor;
                m_CurrentLineRendererReference.endColor = t_ProgressedColor;
            }

            yield return t_CycleDelay;
            t_RemainingTimeForFadingOut -= t_CycleLength;
        }

        t_ProgressedColor = t_ColorGradientOfLineRenderer.Evaluate(1f);

        m_CurrentLineRendererReference.startColor = t_ProgressedColor;
        m_CurrentLineRendererReference.endColor = t_ProgressedColor;

        OnFadingOutEnd?.Invoke();

        StopCoroutine(ControllerForFadingOut(0));

    }

    private IEnumerator ControllerForDelayOnDrawing() {

        float           t_CycleLength   = 0.0167f;
        WaitForSeconds  t_CycleDelay    = new WaitForSeconds(t_CycleLength);

        float   t_RemainingTimeForForDelay = delayForDrawing;
        float   t_ProgressBar;

        Quaternion t_ModifiedRotation;
        Vector3 t_ModifiedPosition = m_DelayIndicatorPosition + positionOffsetForDelayBar;
        delayBarTransformReference.position = t_ModifiedPosition;
        delayBarTransformReference.rotation = Quaternion.LookRotation(m_CameraTransformReference.position - m_PivotForLineDrawing);

        m_IsPointerShowing = true;
        pointerAnimatorReference.gameObject.SetActive(true);
        delayBarAnimatorReference.gameObject.SetActive(true);

        while (m_IsDelayForDrawingControllerRunning) {

            if (t_RemainingTimeForForDelay <= 0)
                break;

            OnChangingPointer(m_DelayIndicatorPosition);

            t_ModifiedRotation = Quaternion.Slerp(
                    delayBarTransformReference.rotation,
                    Quaternion.LookRotation(m_CameraTransformReference.position - m_PivotForLineDrawing),
                    0.2f
                );
            delayBarTransformReference.rotation = t_ModifiedRotation;

            t_ModifiedPosition                  = m_DelayIndicatorPosition  + positionOffsetForDelayBar;
            delayBarTransformReference.position = t_ModifiedPosition;

            t_ProgressBar = 1f - (t_RemainingTimeForForDelay / delayForDrawing);
            delayBarFillerReference.fillAmount = t_ProgressBar;

            yield return t_CycleDelay;
            t_RemainingTimeForForDelay -= t_CycleLength;
        }

        if (t_RemainingTimeForForDelay > 0)
        {
            pointerAnimatorReference.gameObject.SetActive(false);
            //pointerAnimatorReference.SetTrigger("DISAPPEAR");
            m_IsPointerShowing = false;
        }


        m_IsDelayForDrawingControllerRunning = false;
        delayBarAnimatorReference.gameObject.SetActive(false);
        delayBarAnimatorReference.SetTrigger("DISAPPEAR");

        StopCoroutine(ControllerForDelayOnDrawing());

    }

    private bool IsValidIndexOfLineRenderer(int t_LineRendererIndex) {

        if (t_LineRendererIndex >= 0 && t_LineRendererIndex < m_NumberOfLineInTheScene) {

            return true;
        }
        Debug.LogError("Invalid Index for LineRenderer : " + t_LineRendererIndex);
        return false;
    }

    public Vector3[] GetPositionsOfLineRenderer(int t_LineRendererIndex) {

        if (IsValidIndexOfLineRenderer(t_LineRendererIndex)) {

            Vector3[] t_LinePositions = new Vector3[m_ListOfLineRenderers[t_LineRendererIndex].positionCount];
            m_ListOfLineRenderers[t_LineRendererIndex].GetPositions(t_LinePositions);

            return t_LinePositions;
        }

        return new Vector3[0];
    }

#endregion

    #region Public Callback

    public void PreProcess(Vector3 t_PivotForLineDrawing = new Vector3())
    {
        m_PivotForLineDrawing = t_PivotForLineDrawing;

        if (!m_IsLineDrawingControllerRunning)
        {
            if (multiLineDrawingEnabled)
            {

                m_NumberOfLineInTheScene = 0;
                m_ListOfLineRenderers = new List<LineRenderer>();
            }
            else
            {
                m_NumberOfLineInTheScene = 1;
                m_CurrentLineRendererReference = m_OriginLineRendererReference;
            }
            ResetLineRenderer();

            m_IsLineDrawingControllerRunning = true;
            GlobalTouchController.Instance.OnTouchDown  += OnTouchDown;
            GlobalTouchController.Instance.OnTouch      += OnTouch;
            GlobalTouchController.Instance.OnTouchUp    += OnTouchUp;
        }
    }

    public void PostProcess()
    {

        if (m_IsLineDrawingControllerRunning)
        {

            GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
            GlobalTouchController.Instance.OnTouch -= OnTouch;
            GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;
            m_IsLineDrawingControllerRunning = false;
        }
    }

    public void FadingOutLineRenderer(float t_DurationForFadingOut = 0f, UnityAction OnFadingOutEnd = null) {

        pointerAnimatorReference.gameObject.SetActive(false);

        StartCoroutine(ControllerForFadingOut(
            t_DurationForFadingOut == 0 ? defaultDurationForFadingOut : t_DurationForFadingOut, 
            OnFadingOutEnd));
    }

    public void ResetLineRenderer() {

        if (multiLineDrawingEnabled)
        {
            if (multiLineDrawingEnabled)
            {

                int t_NumberOfLineRenderer = m_ListOfLineRenderers.Count;
                for (int i = 0; i < t_NumberOfLineRenderer; i++)
                {

                    Destroy(m_ListOfLineRenderers[i].gameObject);
                }
                m_ListOfLineRenderers.Clear();
            }
        }
        else {

            m_NumberOfLinePosition = 0;
            m_CurrentLineRendererReference.positionCount = 0;
            m_CurrentLineRendererReference.startColor = m_LineRendererGradient.Evaluate(0);
            m_CurrentLineRendererReference.endColor = m_LineRendererGradient.Evaluate(0);

        }

        
    }

    public int GetNumberOfLineInTheScene() {

        return m_NumberOfLineInTheScene;
    }

    public Vector3 GetLatestPositionOfLineRenderer() {

        if (m_CurrentLineRendererReference != null) {

            int t_PositionIndex = m_CurrentLineRendererReference.positionCount - 1;
            if(t_PositionIndex >= 0)
                return m_CurrentLineRendererReference.GetPosition(t_PositionIndex);

        }

        return Vector3.zero;
    }

#endregion

}
