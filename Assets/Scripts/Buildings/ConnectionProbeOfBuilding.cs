﻿using UnityEngine;
using UnityEngine.Events;

public class ConnectionProbeOfBuilding : MonoBehaviour
{

    #region Custom Variables

    public delegate void OnConnectionProbeTouchEvent(ConnectionProbeOfBuilding t_ConnectionProbeOfBuilding);

    #endregion

    #region Public Variables

    //public ParticleSystem psForIdle;
    public Animator indicatorAnimator;

    [HideInInspector]
    public OnConnectionProbeTouchEvent OnConnectionProbeTouch;

    #endregion

    #region Private Variables

    private bool        m_IsConnectionProbeAttached;
    
    #endregion

    #region Public Callback

    public void PreProcess(Color t_Color = new Color()) {
        

        //psForIdle.Play();
        indicatorAnimator.SetTrigger("APPEAR");

        m_IsConnectionProbeAttached = false;

        if (t_Color != new Color(0, 0, 0, 0)) {

            ChangeTheColorOfConnectionProbeParticle(t_Color);
        }
    }

    public void OnRayCastedOnConnectionProbe() {

        if (!m_IsConnectionProbeAttached) {

            //psForIdle.Stop();
            indicatorAnimator.SetTrigger("DISAPPEAR");
            m_IsConnectionProbeAttached = true;

        }

        OnConnectionProbeTouch?.Invoke(this);
    }

    public void ChangeTheColorOfConnectionProbeParticle(Color t_Color) {

        //ParticleSystem.MainModule t_MainModuleReference = psForIdle.main;
        //t_MainModuleReference.startColor = t_Color;
    }

    #endregion
}
