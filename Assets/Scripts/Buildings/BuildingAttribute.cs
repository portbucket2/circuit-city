﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Math;

public class BuildingAttribute : MonoBehaviour
{

    #region Custom Variables

    [System.Serializable]
    public class Connection
    {


        #region Public Variables

        public ConnectionProbeOfBuilding[]              connectionProbeForCompletingCircuit;
        public List<SkinnedMeshRenderer>                lightsToBeIlluminate;
        public List<Animator>                           animatedObjects;

        #endregion

        #region Private Variables

        [HideInInspector]
        public bool         IsLightIlluminatingControllerRunning;

        private Material    m_InstanceMaterial;

        #endregion

        #region Public Callback

        public void PreProcess() {

            int t_NumberOfConnectionProbeOfTheBuilding = connectionProbeForCompletingCircuit.Length;
            for (int i = 0; i < t_NumberOfConnectionProbeOfTheBuilding; i++) {

                connectionProbeForCompletingCircuit[i].PreProcess();
            }

            IsLightIlluminatingControllerRunning = false;
        }

        public void PostProcess(Material t_BloomedMaterial) {

            int t_NumberOfLightToBeIlluminating = lightsToBeIlluminate.Count;
            for (int i = 0; i < t_NumberOfLightToBeIlluminating; i++) {

                lightsToBeIlluminate[i].sharedMaterial = t_BloomedMaterial;
            }

            Destroy(m_InstanceMaterial);
        }

        public void StartAnimation() {

            int t_NumberOfAnimatedObject = animatedObjects.Count;
            for (int i = 0; i < t_NumberOfAnimatedObject; i++) {

                animatedObjects[i].SetTrigger("APPEAR");
            }
        }

        public void StopAnimation()
        {

            int t_NumberOfAnimatedObject = animatedObjects.Count;
            for (int i = 0; i < t_NumberOfAnimatedObject; i++)
            {

                animatedObjects[i].SetTrigger("DISAPPEAR");
            }
        }

        public void ResetLightMaterial(Material t_NonBloomMaterial) {

            int t_NumberOfLight = lightsToBeIlluminate.Count;
            for (int i = 0; i < t_NumberOfLight; i++) {

                lightsToBeIlluminate[i].material = t_NonBloomMaterial;
            }
        }

        public IEnumerator ControllerForIlluminateLights(
            Material t_EmissionMaterial,
            Color t_LightColor,
            float t_DurationOfBlending = 1,
            float t_EmissionIntensity = 1f,
            float t_VariationOfIntensity = 0.25f,
            UnityAction OnIlluminationEnd = null)
        {

            m_InstanceMaterial = Instantiate(t_EmissionMaterial);
            m_InstanceMaterial.EnableKeyword("_EMISSION");
            m_InstanceMaterial.SetColor("_Color", t_LightColor);

            int t_NumberOfMeshToBeIlluminated = lightsToBeIlluminate.Count;
            for (int i = 0; i < t_NumberOfMeshToBeIlluminated; i++)
                lightsToBeIlluminate[i].material = m_InstanceMaterial;


            float t_FixedEmissionIntensity = MathFunction.Instance.GetColorIntensityBasedOnContrast(
                    t_LightColor,
                    t_EmissionIntensity,
                    t_VariationOfIntensity
                );
            float t_Progress;
            float t_RemainingTimeForBlending = t_DurationOfBlending;
            float t_CycleLength = 0.0167f;
            WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

            while (t_RemainingTimeForBlending > 0)
            {

                t_Progress = 1f - (t_RemainingTimeForBlending / t_DurationOfBlending);

                m_InstanceMaterial.SetColor(
                        "_EmissionColor",
                        t_LightColor * Mathf.Lerp(1, 1 + (t_FixedEmissionIntensity - 1), t_Progress)
                    );

                yield return t_CycleDelay;
                t_RemainingTimeForBlending -= t_CycleLength;
            }

            OnIlluminationEnd?.Invoke();
        }

        #endregion

    }

    #endregion

    #region UnityEditor

#if UNITY_EDITOR


    private void OnDrawGizmos()
    {
        if (showConnectionProbe)
        {

            int t_NumberOfGuidedLine = guidedLine.Count;
            for (int i = 0; i < t_NumberOfGuidedLine; i++)
            {

                if (guidedLine[i] != null)
                {

                    Gizmos.color = Color.red;
                    Gizmos.DrawSphere(guidedLine[i].position, 0.1f);
                }

            }
        }

    }

#endif



    #endregion

    #region Public Variables

#if UNITY_EDITOR

    [Header("Reference : Editor")]
    public bool showConnectionProbe;
    public GameObject prefabOfConnectionProbe;
#endif

    [Space(5.0f)]
    [Header("Configuretion  :   Surrounding Visual")]
    public Animator[]                   animatorReferencesForSuroundingEnvironment;

    [Space(5.0f)]
    [Header("Configuretion  :   ConnectionMatrix")]
    public Transform                    cameraPositionReferenceForDrawing;
    public Transform                    cameraPositionReferenceForCompleting;
    public Animator                     animatorReference;
    

    public Connection[]                 newConnectionMatrix;
    public List<Transform>              guidedLine;

    [Space(10.0f)]
    [Header("Configuretion  :   Effect")]
    public ParticleSystem[] celebretionParticles;


    [HideInInspector]
    public UnityAction OnCircuitComplete;

    #endregion

    #region Private Variables

    private int m_IlluminationColorIndex;

    #endregion

    #region Configuretion

    private void AppearEnvironment(int t_EnvironmentIndex) {

        if (!animatorReferencesForSuroundingEnvironment[t_EnvironmentIndex].transform.parent.gameObject.activeInHierarchy)
            animatorReferencesForSuroundingEnvironment[t_EnvironmentIndex].transform.parent.gameObject.SetActive(true);

        animatorReferencesForSuroundingEnvironment[t_EnvironmentIndex].SetTrigger("APPEAR");
    }

    private void DisappearEnvironment(int t_EnvironmentIndex)
    {
        if (animatorReferencesForSuroundingEnvironment[t_EnvironmentIndex].transform.parent.gameObject.activeInHierarchy)
            animatorReferencesForSuroundingEnvironment[t_EnvironmentIndex].SetTrigger("DISAPPEAR");
    }

    private IEnumerator ControllerForPostProcess() {

        int t_NumberOfConnectionMatrix = newConnectionMatrix.Length;
        for (int i = 0; i < t_NumberOfConnectionMatrix; i++) {

            newConnectionMatrix[i].PostProcess(IlluminationController.Instance.GetIlluminateMaterial(m_IlluminationColorIndex));        
        }

        yield return null;

        //DisappearBuilding();

        //yield return new WaitForSeconds(1f);

        //if (gameObject.activeInHierarchy)
        //    gameObject.SetActive(false);
    }

    private IEnumerator ControllerForDisappear() {

        DisappearSurroundingEnvironment(true);

        animatorReference.SetTrigger("DISAPPEAR");

        yield return new WaitForSeconds(1.2f);

        StopCoroutine(ControllerForDisappear());

        gameObject.SetActive(false);
    }

    private IEnumerator ControllerForSequentialAppearAnimationForSurrounding(float t_SequentialSpawnDelay) {

        WaitForSeconds t_SequentialDelay = new WaitForSeconds(t_SequentialSpawnDelay);

        int t_NumberOfSurroundingEnvironment = animatorReferencesForSuroundingEnvironment.Length;
        for (int i = 0; i < t_NumberOfSurroundingEnvironment; i++)
        {
            AppearEnvironment(i);

            yield return t_SequentialDelay;
        }

        StopCoroutine(ControllerForSequentialAppearAnimationForSurrounding(0));
    }

    private IEnumerator ControllerForDisappearAnimationForSurrounding()
    {

        int t_NumberOfSurroundingEnvironment = animatorReferencesForSuroundingEnvironment.Length;
        for (int i = 0; i < t_NumberOfSurroundingEnvironment; i++)
        {
            DisappearEnvironment(i);
        }

        yield return new WaitForSeconds(1);

        for (int i = 0; i < t_NumberOfSurroundingEnvironment; i++)
        {
            animatorReferencesForSuroundingEnvironment[i].transform.parent.gameObject.SetActive(false);
        }

        StopCoroutine(ControllerForDisappearAnimationForSurrounding());
    }

    #endregion

    #region Public Callback

    public void ShowBuildingAsComplete(Material t_BloomedMaterial) {

        AppearBuilding();
        AppearSurroundingEnvironment();
        int t_NumberOfConnection = newConnectionMatrix.Length;
        for (int i = 0; i < t_NumberOfConnection; i++) {

            int t_NumberOfLight = newConnectionMatrix[i].lightsToBeIlluminate.Count;
            for (int j = 0; j < t_NumberOfLight; j++) {

                newConnectionMatrix[i].StartAnimation();
                newConnectionMatrix[i].lightsToBeIlluminate[j].material = t_BloomedMaterial;
            }
        }
    }

    public void AppearBuilding() {

        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        animatorReference.SetTrigger("APPEAR");
    }

    public void DisappearBuilding() {

        StartCoroutine(ControllerForDisappear());
    }

    public void AppearSurroundingEnvironment(bool t_UseSequentialSpawn = false, float t_SequentialSpawnDelay = 0.1f)
    {
        if (gameObject.activeInHierarchy) {

            if (t_UseSequentialSpawn)
            {
                StartCoroutine(ControllerForSequentialAppearAnimationForSurrounding(t_SequentialSpawnDelay));
            }
            else {

                int t_NumberOfSurroundingEnvironment = animatorReferencesForSuroundingEnvironment.Length;
                for (int i = 0; i < t_NumberOfSurroundingEnvironment; i++)
                {
                    AppearEnvironment(i);
                }
            }
        }
    }

    public void DisappearSurroundingEnvironment(bool t_IsDisappearAndDeactivate = false) {

        if (gameObject.activeInHierarchy)
        {

            if (t_IsDisappearAndDeactivate)
            {
                StartCoroutine(ControllerForDisappearAnimationForSurrounding());
            }
            else
            {

                int t_NumberOfSurroundingEnvironment = animatorReferencesForSuroundingEnvironment.Length;
                for (int i = 0; i < t_NumberOfSurroundingEnvironment; i++)
                {
                    DisappearEnvironment(i);
                }
            }
        }
    }

    public void PreProcess(UnityAction OnCircuitComplete, int t_IlluminationColorIndex = 0) {

        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        int t_NumberOfConnectionMatrix = newConnectionMatrix.Length;
        for (int i = 0; i < t_NumberOfConnectionMatrix; i++)
        {
            newConnectionMatrix[i].PreProcess();
        }

        this.OnCircuitComplete                      = OnCircuitComplete;

        m_IlluminationColorIndex                    = t_IlluminationColorIndex;
    }

    public void PostProcess() {

        StartCoroutine(ControllerForPostProcess());
    }

    public void IlluminateLight(int t_IndexOfConnectionMatrix) {

        if (!newConnectionMatrix[t_IndexOfConnectionMatrix].IsLightIlluminatingControllerRunning) {

            newConnectionMatrix[t_IndexOfConnectionMatrix].IsLightIlluminatingControllerRunning = true;

            newConnectionMatrix[t_IndexOfConnectionMatrix].StartAnimation();
            StartCoroutine(newConnectionMatrix[t_IndexOfConnectionMatrix].ControllerForIlluminateLights(
                    IlluminationController.Instance.GetIlluminateMaterial(m_IlluminationColorIndex),
                    IlluminationController.Instance.GetIlluminationColor(m_IlluminationColorIndex), 
                    1,
                    IlluminationController.Instance.GetIlluminationIntensity(m_IlluminationColorIndex),
                    0
                ));
        }
    }

    public void ResetLightMaterialToNonBloomed(Material t_NonBloomMaterial) {

        int t_NumberOfConnection = newConnectionMatrix.Length;
        for (int i = 0; i < t_NumberOfConnection; i++) {

            newConnectionMatrix[i].StopAnimation();
            newConnectionMatrix[i].ResetLightMaterial(t_NonBloomMaterial);
        }
    }

    public void ThrowCelebretionParticle()
    {
        int t_NumberOfCelebretionParticle = celebretionParticles.Length;
        for (int i = 0; i < t_NumberOfCelebretionParticle; i++) {

            celebretionParticles[i].Play();
        }
    }

    #endregion
}
