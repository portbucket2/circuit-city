﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(BuildingAttribute))]
public class BuildingAttributeEditor : Editor
{

    #region Private Variables

    private BuildingAttribute Reference;
    

    #endregion

    #region Editor

    private void OnEnable()
    {
        Reference = (BuildingAttribute)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Config - Connection"))
            {
                ConfigureConnectionProbe();
            }

            if (GUILayout.Button("Config - GuidedLine"))
            {
                ConfigureGuidedLine();
            }

        }
        EditorGUILayout.EndHorizontal();

        
        EditorGUILayout.Space();
        DrawDefaultInspector();



        serializedObject.ApplyModifiedProperties();
    }

    #endregion

    #region Configuretion

    private void ResetConnectionMatrix(int t_TotalNumberOfConnectionMatrix) {

        int t_NumberOfConnectionMatrix = Reference.newConnectionMatrix.Length;
        for (int i = 0; i < t_NumberOfConnectionMatrix; i++) {

            if (Reference.newConnectionMatrix[i].connectionProbeForCompletingCircuit != null) {

                int t_NumberOfConnectionLink = Reference.newConnectionMatrix[i].connectionProbeForCompletingCircuit.Length;
                for (int j = 0; j < t_NumberOfConnectionLink; j++)
                {

                    if(Reference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j] != null)
                        DestroyImmediate(Reference.newConnectionMatrix[i].connectionProbeForCompletingCircuit[j].gameObject);
                }
            }
        }

        Reference.newConnectionMatrix = new BuildingAttribute.Connection[t_TotalNumberOfConnectionMatrix];
        for (int i = 0; i < t_TotalNumberOfConnectionMatrix; i++) {

            Reference.newConnectionMatrix[i] = new BuildingAttribute.Connection();
            Reference.newConnectionMatrix[i].connectionProbeForCompletingCircuit= new ConnectionProbeOfBuilding[2];
            Reference.newConnectionMatrix[i].lightsToBeIlluminate               = new List<SkinnedMeshRenderer>();
        }
    }

    private void ResetGuidedPath() {

        int t_NumberOfGuidedPath = Reference.guidedLine.Count;
        for (int i = 0; i < t_NumberOfGuidedPath; i++) {

            if (Reference.guidedLine[i] != null) {

                DestroyImmediate(Reference.guidedLine[i].gameObject);
            }
        }

        Reference.guidedLine.Clear();
    }

    private Transform FindBuildingWithName(string t_Name, Transform t_Parent) {

        if (t_Parent.name.StartsWith(t_Name))
        {
            return t_Parent;
        }
        else {

            int t_NumberOfChildCount = t_Parent.childCount;
            for (int i = 0; i < t_NumberOfChildCount; i++)
            {
                Transform t_ReturnedValue = FindBuildingWithName(t_Name, t_Parent.GetChild(i));
                if(t_ReturnedValue)
                    return t_ReturnedValue;
            }

            return null;
        }
    }

    private void ConfigureConnectionProbe() {

        Transform t_FBX = FindBuildingWithName("FBX_", Reference.transform);

        if (t_FBX)
        {
            //Configuretion :   ContainerForConnectionProbe
            Transform t_ContainerForConnectionProbe = Reference.transform.Find("Container - ConnectionProbe");
            if (!t_ContainerForConnectionProbe)
            {
                // if : No Container found for guided line
                GameObject t_NewContainerForConnectionProbe = new GameObject();
                t_NewContainerForConnectionProbe.name = "Container - ConnectionProbe";
                t_NewContainerForConnectionProbe.transform.SetParent(Reference.transform);

                t_ContainerForConnectionProbe = t_NewContainerForConnectionProbe.transform;
            }

            Transform t_ContainerForConnectionLink  = t_FBX.Find("ConnectionLink");
            int t_NumberOfConnectionLink            = t_ContainerForConnectionLink.childCount;
            ResetConnectionMatrix(t_NumberOfConnectionLink / 2);
            for (int i = 0; i < t_NumberOfConnectionLink; i++) {

                Mesh t_SharedMesh = t_ContainerForConnectionLink.GetChild(i).GetComponent<SkinnedMeshRenderer>().sharedMesh;
                int t_NumberOfVertex = t_SharedMesh.vertexCount;
                Vector3 t_Center = Vector3.zero;
                for (int j = 0; j < t_NumberOfVertex; j++)
                {

                    t_Center += t_SharedMesh.vertices[j];
                }
                t_Center    /= t_NumberOfVertex;
                t_Center    = t_FBX.TransformPoint(t_Center);

                GameObject t_NewConnectionProbe = Instantiate(
                        Reference.prefabOfConnectionProbe,
                        Reference.prefabOfConnectionProbe.transform.position,
                        Quaternion.identity
                    );
                t_NewConnectionProbe.name = "ConnectionProbe (" + i + ")";
                t_NewConnectionProbe.transform.SetParent(t_ContainerForConnectionProbe);
                t_NewConnectionProbe.transform.position = t_Center;

                Reference.newConnectionMatrix[i / 2].connectionProbeForCompletingCircuit[i % 2] = t_NewConnectionProbe.GetComponent<ConnectionProbeOfBuilding>();
            }

            //Configuretion :   ContainerForLight
            Transform t_ContainerForLights  = t_FBX.Find("Light");
            if(t_ContainerForLights == null)
                t_ContainerForLights = t_FBX.Find("Lights");
            if (t_ContainerForLights == null)
                t_ContainerForLights = t_FBX.Find("LightSource");
            if (t_ContainerForLights == null)
                t_ContainerForLights = t_FBX.Find("LightSources");

            int t_NumberOfLight             = t_ContainerForLights.childCount;
            for (int i = 0; i < t_NumberOfLight; i++) {

                SkinnedMeshRenderer t_SkinMeshRendererReference = t_ContainerForLights.GetChild(i).GetComponent<SkinnedMeshRenderer>();
                Mesh t_SharedMesh = t_SkinMeshRendererReference.sharedMesh;
                int t_NumberOfVertex = t_SharedMesh.vertexCount;
                Vector3 t_Center = Vector3.zero;
                for (int j = 0; j < t_NumberOfVertex; j++)
                {
                    t_Center += t_SharedMesh.vertices[j];
                }
                t_Center /= t_NumberOfVertex;
                t_Center = t_FBX.TransformPoint(t_Center);

                int     t_SelectedConnectionIndex   = 0;
                int     t_NumberOfConnectionMatrix  = Reference.newConnectionMatrix.Length;
                float   t_MinimalDistance           = Mathf.Infinity;
                float   t_CurrentDistance;
                for (int j = 0; j < t_NumberOfConnectionMatrix; j++) {

                    int t_NumberOfConnectedProbe = Reference.newConnectionMatrix[j].connectionProbeForCompletingCircuit.Length;
                    for (int k = 0; k < t_NumberOfConnectedProbe; k++) {

                        t_CurrentDistance = Vector3.Distance(t_Center, Reference.newConnectionMatrix[j].connectionProbeForCompletingCircuit[k].transform.position);
                        if (t_CurrentDistance < t_MinimalDistance) {

                            t_MinimalDistance = t_CurrentDistance;
                            t_SelectedConnectionIndex = j;
                        }
                    }
                }

                Reference.newConnectionMatrix[t_SelectedConnectionIndex].lightsToBeIlluminate.Add(t_SkinMeshRendererReference);
            }

        }
        else {

            Debug.LogError("Cound't find any 'FBX_'. Please add/rename the fbx file");
        }
        
    }

    private void ConfigureGuidedLine()
    {

        Transform t_FBX = FindBuildingWithName("FBX_", Reference.transform);

        if (t_FBX)
        {
            Transform t_ContainerForConnectionProbe = Reference.transform.Find("Container - ConnectionProbe");
            if (t_ContainerForConnectionProbe == null) {

                ConfigureConnectionProbe();
                t_ContainerForConnectionProbe = Reference.transform.Find("Container - ConnectionProbe");
            }

            Transform t_ContainerForGuidedLine = Reference.transform.Find("Container - GuidedLine");
            if (!t_ContainerForGuidedLine)
            {
                // if : No Container found for guided line
                GameObject t_NewContainerForGuidedLine = new GameObject();
                t_NewContainerForGuidedLine.name = "Container - GuidedLine";
                t_NewContainerForGuidedLine.transform.SetParent(Reference.transform);

                t_ContainerForGuidedLine = t_NewContainerForGuidedLine.transform;
            }

            int t_NumberOfConnectionProbe = t_ContainerForConnectionProbe.childCount;
            ResetGuidedPath();
            for (int i = 0; i < t_NumberOfConnectionProbe; i++)
            {
                GameObject t_NewGuidedLine = new GameObject();
                t_NewGuidedLine.name = "GuidedLine (" + i + ")";
                t_NewGuidedLine.transform.SetParent(t_ContainerForGuidedLine);
                t_NewGuidedLine.transform.position = t_ContainerForConnectionProbe.GetChild(i).position;
                Reference.guidedLine.Add(t_NewGuidedLine.transform);
            }
        }
        else {
            Debug.LogError("Cound't find any 'FBX_'. Please add/rename the fbx file");
        }
    }

    #endregion

}
