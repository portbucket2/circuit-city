﻿using UnityEngine;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    #region Custom Variables

    [System.Serializable]
    public class LevelInfo
    {
#if UNITY_EDITOR
        [HideInInspector]
        public string levelName;
#endif

        #region Public Variables

        public BuildingAttribute[] buildingAttributesReferecen;

        #endregion

        #region Private Variables

        private int m_LevelIndex;

        #endregion

        #region Configuretion

        private string GetPlayerPrefKeyForBuilding(int t_BuildingIndex, string t_Extension = "")
        {
            return "Level(" + m_LevelIndex + ")_Building(" + t_BuildingIndex + ")" + (t_Extension == "" ? "" : "_" + t_Extension);
        }

        #endregion

        #region Publi Callback

        public void PreProcess(int t_LevelIndex) {

            m_LevelIndex = t_LevelIndex;
        }

        public void SetLightColorForBuildng(int t_BuildingIndex, int t_ColorIndex) {

            PlayerPrefs.SetInt(GetPlayerPrefKeyForBuilding(t_BuildingIndex, "LightColor"), t_ColorIndex);
        }

        public void IlluminateBuildingForCity(IlluminationController t_IlluminationControllerReference)
        {
            int t_NumberOfBuilding = buildingAttributesReferecen.Length;
            for (int i = 0; i < t_NumberOfBuilding; i++) {

                buildingAttributesReferecen[i].ShowBuildingAsComplete(t_IlluminationControllerReference.GetIlluminateMaterial(GetLightColorForBuilding(i)));
            }
        }

        public int GetLightColorForBuilding(int t_BuildingIndex) {

            return PlayerPrefs.GetInt(GetPlayerPrefKeyForBuilding(t_BuildingIndex, "LightColor"), 0);
        }

        #endregion
        

    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR

    [Space(5.0f)]
    [Header("Configuretion  :   Editor")]
    public Transform levelContainer;

#endif

    [Space(5.0f)]
    [Header("Reference      :   External")]
    public Material                 nonBloomedLightMaterial;
    public IlluminationController   illuminationControllerReference;

    [Space(5.0f)]
    [Header("Configuretion  :   Level")]
    public LevelInfo[] levelInfos;

    #endregion

    #region Private Variables

    private string TRACKER_FOR_LEVEL = "TRACKER_FOR_LEVEL";

    #endregion

    #region MonoBehaviour

#if UNITY_EDITOR

    private void OnValidate()
    {
        int t_NumberOfLevelInfo = levelInfos.Length;
        for (int i = 0; i < t_NumberOfLevelInfo; i++)
        {

            levelInfos[i].levelName = "Level " + i;
        }
    }

#endif

    private void Awake()
    {
        PreProcess();
    }

    #endregion

    #region Configuretion

    public void PreProcess() {

        int t_NumberOfLevel= levelInfos.Length;
        int t_CurrentLevel = GetCurrentLevel();
        for (int i = 0; i < t_NumberOfLevel; i++) {

            levelInfos[i].PreProcess(i);
            if (i < t_CurrentLevel) {

                levelInfos[i].IlluminateBuildingForCity(illuminationControllerReference);
            }

        }
    }

    #endregion

    #region Public Callback

    public void     IncreaseLevel() {

        int t_MaxLevel      = levelInfos.Length;
        int t_CurrentLevel  = GetCurrentLevel();

        if ((t_CurrentLevel + 1) < t_MaxLevel)
        {
            PlayerPrefs.SetInt(TRACKER_FOR_LEVEL, t_CurrentLevel + 1);
        }
        else 
        {
            ResetLevel();
        }
    }

    public void     ResetLevel() {

        int t_NumberOfLevel = levelInfos.Length;
        for (int i = 0; i < t_NumberOfLevel; i++)
        {
            int t_NumberOfBuilding = levelInfos[i].buildingAttributesReferecen.Length;
            for (int j = 0; j < t_NumberOfBuilding; j++) {

                levelInfos[i].buildingAttributesReferecen[j].ResetLightMaterialToNonBloomed(nonBloomedLightMaterial);
                levelInfos[i].buildingAttributesReferecen[j].DisappearBuilding();
            }
            
        }

        PlayerPrefs.SetInt(TRACKER_FOR_LEVEL, 0);
    }

    public int      GetCurrentLevel()
    {
        return PlayerPrefs.GetInt(TRACKER_FOR_LEVEL, 0);
    }

    public int GetMaxLevel() {

        return levelInfos.Length;
    }

    public float    GetLevelProgression() {

        return GetCurrentLevel() / (1f * levelInfos.Length);
    }

    public List<Transform> GetTransformReferenceOfCompletedLevel() {

        List<Transform> t_ListOfTransformReferenceOfBuilding = new List<Transform>();

        int t_CurrentLevel = GetCurrentLevel();
        for (int i = 0; i <= t_CurrentLevel; i++) {

            int t_NumberOfBuildingOnLevel = levelInfos[i].buildingAttributesReferecen.Length;
            for (int j = 0; j < t_NumberOfBuildingOnLevel; j++) {

                t_ListOfTransformReferenceOfBuilding.Add(levelInfos[i].buildingAttributesReferecen[j].transform);
            }
        }

        return t_ListOfTransformReferenceOfBuilding;
    }

    #endregion

}
