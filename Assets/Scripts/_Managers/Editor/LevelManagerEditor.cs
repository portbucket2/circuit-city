﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor
{

    private LevelManager Reference;

    private void OnEnable()
    {
        Reference = (LevelManager)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.LabelField("CurrentLevel (" + Reference.GetCurrentLevel() + ") : ");

            if (GUILayout.Button("Level Up")) {

                Reference.IncreaseLevel();
            }

            if (GUILayout.Button("Reset"))
            {
                PlayerPrefs.SetInt("TRACKER_FOR_LEVEL", 0);
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Show Levels")) {

                ShowLevel();
            }

            if (GUILayout.Button("Hide Levels"))
            {
                HideLevel();
            }
        }
        EditorGUILayout.EndHorizontal();

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    private void SetLevelStatus(Transform t_Parent, bool t_LevelStatus = true)
    {

        int t_ChildCount = t_Parent.childCount;
        for (int i = 0; i < t_ChildCount; i++)
        {
            if (!t_Parent.GetChild(i).GetComponent<BuildingAttribute>())
            {

                string t_ChildName = t_Parent.GetChild(i).name;
                if (t_ChildName.StartsWith("Level - ")
                 || t_ChildName.StartsWith("Environment"))
                {
                    t_Parent.GetChild(i).gameObject.SetActive(true);
                }
                else
                {

                    t_Parent.GetChild(i).gameObject.SetActive(t_LevelStatus);
                }

                if(!t_ChildName.StartsWith("Props_"))
                    SetLevelStatus(t_Parent.GetChild(i), t_LevelStatus);
            }
            else {

                t_Parent.GetChild(i).gameObject.SetActive(t_LevelStatus);
            }
            
        }
    }

    private void ShowLevel()
    {
        SetLevelStatus(Reference.levelContainer, true);
    }

    private void HideLevel()
    {
        SetLevelStatus(Reference.levelContainer, false);
    }

}
