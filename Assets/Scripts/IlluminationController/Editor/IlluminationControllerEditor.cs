﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(IlluminationController))]
public class IlluminationControllerEditor : Editor
{

    private IlluminationController Reference;

    private void OnEnable()
    {
        Reference = (IlluminationController)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.Space();
        CustomInspector();
        EditorGUILayout.Space();
        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    private int GetColorIndexForDisplay() {

        return PlayerPrefs.GetInt("DISPLAY_COLOR_INDEX", 0);
    }

    private void SetColorIndexForDisplay(int t_ColorIndex) {

        PlayerPrefs.SetInt("DISPLAY_COLOR_INDEX", t_ColorIndex);
    }

    private void CustomInspector() {

        EditorGUILayout.BeginHorizontal();{

            if (GUILayout.Button("Add Color"))
            {

                Reference.illuminateColor.Add(new IlluminationController.IlluminateColor()
                {
                    color = Reference.defaultEmissionColor,
                    intensity = Reference.defaultEmissionIntensity
                });
            }

            if (GUILayout.Button("Display Color (" + GetColorIndexForDisplay() + ")")) {

                int t_ColorIndex = GetColorIndexForDisplay();
                if (t_ColorIndex >= 0 && t_ColorIndex < Reference.illuminateColor.Count) {

                    Reference.defaultEmissionMaterial.SetColor("_Color", Reference.illuminateColor[t_ColorIndex].color);
                    Reference.defaultEmissionMaterial.SetColor("_EmissionColor", Reference.illuminateColor[t_ColorIndex].color * Reference.illuminateColor[t_ColorIndex].intensity);
                }

                t_ColorIndex++;

                if (t_ColorIndex >= Reference.illuminateColor.Count)
                {
                    SetColorIndexForDisplay(0);
                }
                else
                {
                    SetColorIndexForDisplay(t_ColorIndex);
                }

                
            }
        }
        EditorGUILayout.EndHorizontal();

        
    }
}
