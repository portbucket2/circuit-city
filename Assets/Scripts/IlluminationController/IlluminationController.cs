﻿using UnityEngine;
using System.Collections.Generic;

public class IlluminationController : MonoBehaviour
{
    #region Custom Variables

    [System.Serializable]
    public struct IlluminateColor
    {
        public Material presetMaterial;
        public Color color;
        [Range(1f, 10.0f)]
        public float intensity;
    }

    #endregion

    #region Public Variables

    public static IlluminationController Instance;

    [Header("Reference  :   Default (Test) Values")]
    public Material     defaultEmissionMaterial;
    public Color        defaultEmissionColor;
    [Range(1f, 25.0f)]
    public float        defaultEmissionIntensity;

    
    [Space(5.0f)]
    public List<IlluminateColor> illuminateColor;

    #endregion

    #region Private Variables


    #endregion

    #region Mono Behaviour

    private void OnEnable()
    {
        if (defaultEmissionMaterial != null) {

            defaultEmissionMaterial.EnableKeyword("_EMISSION");
            defaultEmissionMaterial.SetColor("_Color", defaultEmissionColor);
        }
    }

    private void OnValidate()
    {
        if (defaultEmissionMaterial != null) {

            defaultEmissionMaterial.SetColor("_Color", defaultEmissionColor);
            defaultEmissionMaterial.SetColor("_EmissionColor", defaultEmissionColor * defaultEmissionIntensity);
        }

        int t_NumberOfIlluminateColor = illuminateColor.Count;
        for (int i = 0; i < t_NumberOfIlluminateColor; i++) {

            if (illuminateColor[i].presetMaterial != null) {

                illuminateColor[i].presetMaterial.SetColor("_Color", illuminateColor[i].color);
                illuminateColor[i].presetMaterial.SetColor("_EmissionColor", illuminateColor[i].color * illuminateColor[i].intensity);
            }
        }
            
    }

    private void Awake()
    {
        if (Instance == null) {

            Instance = this;
        }
    }
    #endregion

    #region Configuretion

    private bool IsValidIndex(int t_IlluminateColorIndex) {

        if (t_IlluminateColorIndex >= 0 && t_IlluminateColorIndex < illuminateColor.Count) {

            return true;
        }

        return false;
    }

    #endregion

    #region Public Callback

    public int GetRandomIlluminationIndex() {

        return Random.Range(0, illuminateColor.Count);
    }

    public float GetRandomIlluminationIntensity() {

        return GetIlluminationIntensity(GetRandomIlluminationIndex());
    }

    public float GetIlluminationIntensity(int t_IlluminateColorIndex) {

        if (IsValidIndex(t_IlluminateColorIndex)) {

            return illuminateColor[t_IlluminateColorIndex].intensity;
        }

        return defaultEmissionIntensity;
    }

    public Color GetRandomIlluminationColor() {

        return  GetIlluminationColor(GetRandomIlluminationIndex());
    }

    public Color GetIlluminationColor(int t_IlluminateColorIndex) {

        if (IsValidIndex(t_IlluminateColorIndex)) {

            return illuminateColor[t_IlluminateColorIndex].color;
        }

        return defaultEmissionColor;
    }

    public Material GetRandomIlluminationMaterial() {

        return  GetIlluminateMaterial(GetRandomIlluminationIndex());
    }

    public Material GetIlluminateMaterial(int t_IlluminateColorIndex) {

        if (IsValidIndex(t_IlluminateColorIndex)) {

            return illuminateColor[t_IlluminateColorIndex].presetMaterial;
        }

        return defaultEmissionMaterial;
    }

    #endregion
}
