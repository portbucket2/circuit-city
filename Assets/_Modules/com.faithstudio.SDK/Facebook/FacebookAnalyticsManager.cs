﻿namespace com.faithstudio.SDK
{
    using UnityEngine;
    using Facebook.Unity;

    public class FacebookAnalyticsManager : MonoBehaviour
    {

        public static FacebookAnalyticsManager Instance;

        void Awake()
        {

            if (Instance == null)
            {

                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        #region Public Callback

        public void FBALevelStart(int t_CurrentLevel)
        {
            Debug.Log("FBCallback : " + "Level Start");
            FB.LogAppEvent(
                "Level Start",
                (float)t_CurrentLevel,
                null
            );

        }

        public void FBALevelComplete(int t_CurrentLevel)
        {
            Debug.Log("FBCallback : " + "Level Achieved");
            FB.LogAppEvent(
                "Level Achieved",
                (float)t_CurrentLevel,
                null
            );

        }

        public void FBRewardedVideoAd(string t_AdType)
        {

            FB.LogAppEvent(
                "RewardedVideoAd_" + t_AdType,
                1,
                null
            );
        }

        #endregion
    }
}


