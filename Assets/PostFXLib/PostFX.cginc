#ifndef POSTFX_INCLUDED
#define POSTFX_INCLUDED

#include "PostFXShaderVariable.cginc"

    kFloat2 GetUV(kFloat4 c)//used for sampling 2D texture for LUT
    {
        //c.g = 1 - saturate(c.g);

        kFloat b = floor(c.b * _Dim * _Dim);
        kFloat by = floor(b / _Dim);
        kFloat bx = floor(b - by * _Dim);

        kFloat2 uv = c.rg * _ScaleRG + _Offset;
        uv += kFloat2(bx, by) / _Dim;

        return uv;
    }

    kFloat4 sampleAs3DTexture(sampler2D lutTex, kFloat3 uv, kFloat width) 
    {

        uv.x = saturate(uv.x) * (1.0 - (_borderAreaLUT * 2) / (width + _widthAddLUT)) + _borderAreaLUT / (width + _widthAddLUT);
        uv.x = clamp(uv.x, 0.5 / width, (width - 0.5) / width);
        uv.y = 1.0 - uv.y;
     
        kFloat sliceBase = saturate(uv.z) * (width - 1.0001);
        kFloat sliceFrac = frac(sliceBase);
        kFloat2 slice0 = kFloat2((sliceBase - sliceFrac + uv.x) / width, uv.y);
        kFloat2 slice1 = kFloat2((sliceBase - sliceFrac + 1.0 + uv.x) / width, uv.y);
     
        kFloat4 slice0Color = tex2D(lutTex, slice0);
        kFloat4 slice1Color = tex2D(lutTex, slice1);
     
        kFloat4 lerpedColor = lerp(slice0Color, slice1Color, sliceFrac);
        //lerpedColor.r -= 0.1098039215686275;//hack
        //lerpedColor.g -= 0.0313725490196078; //hack
        lerpedColor.b -= _BlueCorrection / 255;

        return lerpedColor;
    }

    
    kFloat4  SharpSceneImage (sampler2D mainTexScene, kFloat2 uv, kFloat4 colorTillNow)
    {
        kFloat4 res = kFloat4(0,0,0,0);
        kFloat4 original = colorTillNow;

        kFloat2 offset[8] =
        {
            kFloat2 (1.0, 1.0),
            kFloat2 (-1.0, -1.0),
            kFloat2 (-1.0, 1.0),
            kFloat2 (1.0, -1.0),

            kFloat2 (1.41, 0.0),
            kFloat2 (-1.41, 0.0),
            kFloat2 (0.0, 1.41),
            kFloat2 (0.0, -1.41)
        };

        int i = 0;

        kFloat4 tcol = original;
        kFloat2 inv_screen_size = 1.0 / kFloat2(_ScreenSize.xy);

        for (i = 0; i < 8; i++)//raise 8 to 16 for example for quality increase
        {
            kFloat2 tdir = offset[i].xy;
            uv.x = uv.x + tdir.x * inv_screen_size.x * _sharp_range;
            uv.y = uv.y + tdir.y * inv_screen_size.y * _sharp_range;
            kFloat4 ct = tex2D (mainTexScene, uv);
            tcol += ct;
        }

        tcol *= 0.1111;//or put 0.2 or higher for lesser quality
       // return tcol;
        //Sharp by color
        res = original * (1.0 + ((original - tcol) * _sharp_amount));


        //Sharp by gray
        // float diff_fact = dot ((original.xyz - tcol.xyz), 0.333);
        // res = original * (1.0 + diff_fact * _sharp_amount);

        // Less sharpening for bright pixels
        kFloat r_gray = original.z;
        r_gray = pow (r_gray, 3.0);
        res = lerp (res, original, saturate (r_gray));
        res.w = 1.0;
        return res;
    }


	kFloat RGBToL(kFloat3 colorArg)
    {
        kFloat fmin = min(min(colorArg.r, colorArg.g), colorArg.b);    //Min. value of RGB
        kFloat fmax = max(max(colorArg.r, colorArg.g), colorArg.b);    //Max. value of RGB
        
        return (fmax + fmin) / 2.0; // Luminance
    }
 
    kFloat3 RGBToHSL(kFloat3 colorArg)
    {
        kFloat3 hsl; // init to 0 to avoid warnings ? (and reverse if + remove first part)

        kFloat fmin = min(min(colorArg.r, colorArg.g), colorArg.b);    //Min. value of RGB
        kFloat fmax = max(max(colorArg.r, colorArg.g), colorArg.b);    //Max. value of RGB
        kFloat delta = fmax - fmin;             //Delta RGB value

        hsl.z = (fmax + fmin) / 2.0; // Luminance

        if (delta == 0.0)		//This is a gray, no chroma...
        {
            hsl.x = 0.0;	// Hue
            hsl.y = 0.0;	// Saturation
        }
        else                                    //Chromatic data...
        {
            if (hsl.z < 0.5)
                hsl.y = delta / (fmax + fmin); // Saturation
            else
                hsl.y = delta / (2.0 - fmax - fmin); // Saturation
            
            kFloat deltaR = (((fmax - colorArg.r) / 6.0) + (delta / 2.0)) / delta;
            kFloat deltaG = (((fmax - colorArg.g) / 6.0) + (delta / 2.0)) / delta;
            kFloat deltaB = (((fmax - colorArg.b) / 6.0) + (delta / 2.0)) / delta;
            
            if (colorArg.r == fmax )
                hsl.x = deltaB - deltaG; // Hue
            else if (colorArg.g == fmax)
                hsl.x = (1.0 / 3.0) + deltaR - deltaB; // Hue
            else if (colorArg.b == fmax)
                hsl.x = (2.0 / 3.0) + deltaG - deltaR; // Hue
            
            if (hsl.x < 0.0)
                hsl.x += 1.0; // Hue
            else if (hsl.x > 1.0)
                hsl.x -= 1.0; // Hue
        }
        return hsl;
    }

    kFloat HueToRGB(kFloat f1, kFloat f2, kFloat hue)
    {
        if (hue < 0.0)
            hue += 1.0;
        else if (hue > 1.0)
            hue -= 1.0;
        kFloat res;
        if ((6.0 * hue) < 1.0)
            res = f1 + (f2 - f1) * 6.0 * hue;
        else if ((2.0 * hue) < 1.0)
            res = f2;
        else if ((3.0 * hue) < 2.0)
            res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
        else
            res = f1;
        return res;
    }

    kFloat3 HSLToRGB(kFloat3 hsl)
    {
        kFloat3 rgb;

        if (hsl.y == 0.0)
            rgb = hsl.zzz; // Luminance
        else
        {
            kFloat f2;
            
            if (hsl.z < 0.5)
                f2 = hsl.z * (1.0 + hsl.y);
            else
                f2 = (hsl.z + hsl.y) - (hsl.y * hsl.z);
            
            kFloat f1 = 2.0 * hsl.z - f2;
            
            rgb.r = HueToRGB(f1, f2, hsl.x + (1.0/3.0));
            rgb.g = HueToRGB(f1, f2, hsl.x);
            rgb.b = HueToRGB(f1, f2, hsl.x - (1.0/3.0));
        }
        return rgb;
    }

   



#endif