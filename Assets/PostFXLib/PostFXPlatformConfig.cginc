#ifndef POSTFX_PLATFORM_SPECIFIC_UNIFORM_INCLUDED
#define POSTFX_PLATFORM_SPECIFIC_UNIFORM_INCLUDED

#if defined(_HighPrecision)
#define kFloat float
#define kFloat2 float2
#define kFloat3 float3
#define kFloat4 float4
#define kFloat2x2 float2x2
#define KFloat3x3 float3x3
#define KFloat4x4 float4x4

#elif defined(_MedPrecision)
#define kFloat half
#define kFloat2 half2
#define kFloat3 half3
#define kFloat4 half4
#define kFloat2x2 half2x2
#define KFloat3x3 half3x3
#define KFloat4x4 half4x4

#else //defined(_LowPrecision)
#define kFloat fixed
#define kFloat2 fixed2
#define kFloat3 fixed3
#define kFloat4 fixed4
#define kFloat2x2 fixed2x2
#define KFloat3x3 fixed3x3
#define KFloat4x4 fixed4x4
#endif



#endif