﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PostProcessKai : MonoBehaviour {

    public Material postFXmaterial;

    public bool tvScanLineAndFilmGrain = false;
    public bool grayscale = false;
    public bool vignetting = false;
    public bool sepia = false;
    public bool brightnessAndContrast = false;
    public bool LUTbasedColorCorrection = false;
    public bool levels = false;
    public bool screenBurn = false;
    public bool invert = false;
    public bool colorBalance = false;
    public bool vibrance = false;
    public bool sharpen = false;
    public bool exposure = false;
    public bool channelMixer = false;
    public bool fade = false;
    public bool twirl = false;

    public enum POSTFX_precision { HighPrecision, MediumPrecision, LowPrecision };
    public POSTFX_precision postFXquality = POSTFX_precision.HighPrecision;
    //public Texture2D LutOriginal, converted2DLut;
	// Use this for initialization
	void Start () {
        
        postFXmaterial.SetTexture("_MainTex", null);

        //normally you do not want to select post processing effect in runtime, so we call it here in start.
        //however, if you change any of the property be sure to call OnChangeProps(). In future, these all will be replaced by elegant editor plugin
        ManageFeatures();
        
	}

    
    public void ManageFeatures()
    {
        if (tvScanLineAndFilmGrain)
        {
            Shader.EnableKeyword("_ScanlineAndFilmGrainON");
        }
        else
        {
            Shader.DisableKeyword("_ScanlineAndFilmGrainON");
        }

        if (grayscale)
        {
            Shader.EnableKeyword("_GrayScaleON");
        }
        else
        {
            Shader.DisableKeyword("_GrayScaleON");
        }

        if (vignetting)
        {
            Shader.EnableKeyword("_VignettingON");
        }
        else
        {
            Shader.DisableKeyword("_VignettingON");
        }

        if (sepia)
        {
            Shader.EnableKeyword("_SepiaON");
        }
        else
        {
            Shader.DisableKeyword("_SepiaON");
        }

        if (brightnessAndContrast)
        {
            Shader.EnableKeyword("_BrightnessContrast");
        }
        else
        {
            Shader.DisableKeyword("_BrightnessContrast");
        }

        if (LUTbasedColorCorrection)
        {
            Shader.EnableKeyword("_LUTcolorGrading");
        }
        else
        {
            Shader.DisableKeyword("_LUTcolorGrading");
        }

        if (levels)
        {
            Shader.EnableKeyword("_Level");
        }
        else
        {
            Shader.DisableKeyword("_Level");
        }
        if (screenBurn)
        {
            Shader.EnableKeyword("_ScreenBurn");
        }
        else
        {
            Shader.DisableKeyword("_ScreenBurn");
        }

        if (invert)
        {
            Shader.EnableKeyword("_Invert");
        }
        else
        {
            Shader.DisableKeyword("_Invert");
        }

        if (colorBalance)
        {
            Shader.EnableKeyword("_colorBalance");
        }
        else
        {
            Shader.DisableKeyword("_colorBalance");
        }

        if (vibrance)
        {
            Shader.EnableKeyword("_Vibrance");
        }
        else
        {
            Shader.DisableKeyword("_Vibrance");
        }
        if (sharpen)
        {
            Shader.EnableKeyword("_Sharp");
            postFXmaterial.SetVector("_ScreenSize", new Vector4(Screen.width, Screen.height, 0, 0));
        }
        else
        {
            Shader.DisableKeyword("_Sharp");
        }

        if (exposure)
        {
            Shader.EnableKeyword("_Exposure");
        }
        else
        {
            Shader.DisableKeyword("_Exposure");
        }
        //Shader.EnableKeyword("_HighPrecision");
        if (postFXquality == POSTFX_precision.HighPrecision)
        {
            Shader.EnableKeyword("_HighPrecision");

            Shader.DisableKeyword("_MedPrecision");
            Shader.DisableKeyword("_LowPrecision");
        }
        else if (postFXquality == POSTFX_precision.MediumPrecision)
        {
            Shader.EnableKeyword("_MedPrecision");

            Shader.DisableKeyword("_HighPrecision");
            Shader.DisableKeyword("_LowPrecision");
        }
        else
        {
            Shader.EnableKeyword("_LowPrecision");

            Shader.DisableKeyword("_HighPrecision");
            Shader.DisableKeyword("_MedPrecision");
        }

        if (channelMixer)
        {
            Shader.EnableKeyword("_ChannelMixer");
        }
        else
        {
            Shader.DisableKeyword("_ChannelMixer");
        }

        if (fade)
        {
            Shader.EnableKeyword("_Fade");
        }
        else
        {
            Shader.DisableKeyword("_Fade");
        }

        if (twirl)
        {
            Shader.EnableKeyword("_twirl");
        }
        else
        {
            Shader.DisableKeyword("_twirl");
        }
    }
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
        ManageFeatures();//as we need to preview on editor but we never want to do this on runtime, so this check.
#endif
    
	}

    public void OnChangeProps()//this must be called when you change any effect in runtime
    {
        ManageFeatures();
    }

    void OnRenderImage(RenderTexture rendSRC, RenderTexture rendDest)
    {
        if (postFXmaterial == null)
        {
            Debug.Log("please assign a material which has postFXMain shader on it!");
            return;        
        }

        Graphics.Blit(rendSRC, rendDest, postFXmaterial);
    }
}
