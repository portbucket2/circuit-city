﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.PostEffect
{
    [ExecuteInEditMode]
    public class BloomKai : MonoBehaviour
    {
        [SerializeField] float delta;
        [Range(0.00001f, 2f)] [SerializeField] float bloomRev;

        void Start()
        {
            postFXmaterial.SetTexture("_MainTex", null);
            postFXmaterial.SetFloat("delta", delta);
            postFXmaterial.SetFloat("bloomRev", bloomRev);

        }

#if UNITY_EDITOR
        private void Update()
        {
            postFXmaterial.SetFloat("delta", delta);
            postFXmaterial.SetFloat("bloomRev", bloomRev);
        }
#endif

        [SerializeField] Material postFXmaterial;
        void OnRenderImage(RenderTexture rendSRC, RenderTexture rendDest)
        {
            if (postFXmaterial == null)
            {
                Debug.Log("please assign a material which has postFXMain shader on it!");
                return;
            }

            Graphics.Blit(rendSRC, rendDest, postFXmaterial);
        }
    }

}