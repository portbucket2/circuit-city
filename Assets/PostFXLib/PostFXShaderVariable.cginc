#ifndef POSTFX_SHADER_VARIABLE_INCLUDED
#define POSTFX_SHADER_VARIABLE_INCLUDED

uniform kFloat4 _MainTex_TexelSize;

uniform sampler2D _MainTex;


//#if defined(_VignettingON)
	uniform kFloat _VignetteDirX;
	uniform kFloat _VignetteDirY;
//#endif

//#if defined(_BrightnessContrast)
	uniform kFloat _Contrast;
	uniform kFloat _Brightness;
//#endif

//#if defined(_SepiaON)
	uniform kFloat _brownsepia;
	uniform kFloat _greensepia;
	uniform kFloat _bluesepia;
//#endif

//#if defined(_ScanlineAndFilmGrainON)
	uniform kFloat _ScanlineAmount;
	uniform kFloat _ScanlineCount;
	uniform kFloat _FilmgrainAmount;
//#endif

//#if defined(_LUTcolorGrading)
	uniform sampler2D _LutTex;
	uniform kFloat _ScaleRG;
	uniform kFloat _Offset;
	uniform kFloat _Dim;

	uniform kFloat _borderAreaLUT;
	uniform kFloat _widthAddLUT;
	uniform kFloat _BlueCorrection;
	uniform sampler1D _OneDimLUTtex;
//#endif

//#if defined(_Level)
	uniform kFloat _MaxLevelInput;
	uniform kFloat _MinLevelInput;
	uniform kFloat _MaxLevelOutput;
	uniform kFloat _MinLevelOutput;
	uniform kFloat _GammaLevel;
//#endif

//#if defined(_ScreenBurn)
	uniform sampler2D _BeforeBurnTex;
	uniform sampler2D _BurnTex;
	uniform kFloat _burnThreshold;
//#endif


//#if defined(_colorBalance)
	uniform kFloat _redShift;
	uniform kFloat _greenShift;
	uniform kFloat _blueShift;
//#endif


//uniform fixed _vibrance_ext_night;
//uniform fixed _vibrance_ext_day;
//uniform fixed _vibrance_int_night;
//uniform fixed _vibrance_int_day;
//uniform fixed _ENightDayFactor;
//uniform fixed _EInteriorFactor;

//#if defined(_Vibrance)
	uniform kFloat _VibranceAmount;
	uniform kFloat _SaturationAmount;
//#endif

//#if defined(_Sharp)
	uniform kFloat _sharp_amount;
	uniform kFloat2 _ScreenSize;
	uniform kFloat _sharp_range;
//#endif

//#if defined(_Exposure)
	uniform kFloat _ExposureAmount;
	uniform kFloat _OffsetExposure;
	uniform kFloat _GammaCorrectionExposure;
//#endif

uniform kFloat4 _RedAmountChannelMixer;
uniform kFloat4 _GreenAmountChannelMixer;
uniform kFloat4 _BlueAmountChannelMixer;
uniform kFloat4 _GrayAmountChannelMixer;
uniform kFloat _Desaturation;

uniform kFloat4 _FadeColor;
uniform kFloat _FadeAmount;


uniform kFloat _TwirlAmount;
uniform kFloat _TwirlSize;
uniform kFloat _TwirlRadius;
uniform kFloat2 _TwirlOrigin;


#endif