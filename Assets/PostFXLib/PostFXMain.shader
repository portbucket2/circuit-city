///Credits:
///For film grain, scanline and sepia color correction: 
/////http://forum.thegamecreators.com/?m=forum_view&b=24&t=172965&p=0### bond1 from thegamecreators.com, thank you!
///as it is still working in progress, proper credit text will be managed and placed appropriately.

Shader "Kaiyum/PostFX/PostFXMain" {
Properties {
	_MainTex ("Screen Texture(populated by engine)", 2D) = "white" {}
	//_EffectMaskTex("Effect mask texture, r channel for noise, g channel for blur", 2D) = "white" {}
	_VignetteDirX("Vignette direction X", range(0,1)) = 0.2
	_VignetteDirY("Vignette direction Y", range(0,1)) = 0.2
	_brownsepia("Brown Sepia", range(0,1)) = 0 
	_greensepia("Green Sepia", range(0,1)) = 0 
	_bluesepia("Blue Sepia", range(0,1)) = 0
	_ScanlineAmount("Scanline Amount", float) = 0.01 //range(0.1,10)) = 0.1
	_ScanlineCount("Scanline count", float) = 1024
	_FilmgrainAmount("Film grain", range(0.1, 1)) = 0.15
	_Contrast("Contrast", range(0,5)) = 1 
	_Brightness("Brightness", range(-1, 2)) = 0
	_LutTex("Lut texture for color correction", 2D) = "white"{}

	_MaxLevelInput("Max Level input", range(0,1)) = 0.2
	_MinLevelInput("Min Level input", range(0,1)) = 0.2

	_MaxLevelOutput("Max Level output", range(0,1)) = 0.2
	_MinLevelOutput("Min Level output", range(0,1)) = 0.2

	_GammaLevel("Gamma level", range(0,10)) = 2
	_BeforeBurnTex("before burn texture", 2D) = "white" {}
	_BurnTex("Burn texture", 2D) = "white" {}
	_burnThreshold("Burn threshold amount", range(0,1)) = 0.4
	_borderAreaLUT("LUT adjustment area", float) = 0.4
	_widthAddLUT("Width add LUT", float) = 0.4
	_BlueCorrection("Blue correction", float) = 8
	_OneDimLUTtex("one dimentional LUT", 2D) = "white"{}

	_redShift("redShift amount", range(-10, 10)) = 0.2
	_greenShift("greenShift amount", range(-10, 10)) = 0.2
	_blueShift("blueShift amount", range(-10, 10)) = 0.2
	_VibranceAmount("vibrance amount", float) = 1 //range(-1, 1)) = 0
	_SaturationAmount("Saturation amount", float) = 1 //range(0, 1)) = 0

	_sharp_amount("Amount of sharpness", range(0,5)) = 2
	_sharp_range("Sharp range", range(0,1)) = 0.3


	_ExposureAmount("Exposure amount", float) = 0
	_OffsetExposure("Offset of exposure amount", float) = 0
	_GammaCorrectionExposure("Gamma correction of Exposure effect", float) = 0

	//_ScreenSize("Screen Size", vector) = (1024,512,0,0)


    //http://001.vade.info/?page_id=20
    //channel mixer photoshop
	_RedAmountChannelMixer("Red Amount for channel mixer", color) = (1,0,0,1)
	_GreenAmountChannelMixer("Green Amount for channel mixer", color) = (0,1,0,1)
	_BlueAmountChannelMixer("Blue Amount for channel mixer", color) = (0,0,1,1)
	_GrayAmountChannelMixer("Gray Amount for channel mixer", color) = (0.33,0.33,0.33,0.33)
	_Desaturation("Desaturation Amount for channel mixer", range(0,1)) = 0.1


	_FadeColor("Fade Color", color) = (0,1,0.2,1)
	_FadeAmount("Fade Amount", range(0,1)) = 0.7

	_TwirlAmount("Twirl Amount", float) = 0.2
	_TwirlSize("Twirl Size", float) = 0.2
	_TwirlOrigin("Twirl Origin", vector) = (1,1,0,0)
	_TwirlRadius("Twirl Radius", float) = 0.2

}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
				
		CGPROGRAM
		#pragma vertex vert_img
		#pragma fragment frag
		#pragma target 3.0
		#pragma multi_compile __ _ScanlineAndFilmGrainON
		#pragma multi_compile __ _GrayScaleON
		#pragma multi_compile __ _VignettingON
		#pragma multi_compile __ _SepiaON
		#pragma multi_compile __ _BrightnessContrast
		#pragma multi_compile __ _LUTcolorGrading
		#pragma multi_compile __ _Level
		#pragma multi_compile __ _ScreenBurn
		#pragma multi_compile __ _Invert
		#pragma multi_compile __ _colorBalance
		#pragma multi_compile __ _Vibrance
		#pragma multi_compile __ _Sharp
		#pragma multi_compile _HighPrecision _MedPrecision _LowPrecision
		#pragma multi_compile __ _Exposure
		#pragma multi_compile __ _ChannelMixer
		#pragma multi_compile __ _Fade
		#pragma multi_compile __ _twirl

		#include "PostFXConfig.cginc"//We define all necessary macro properly
		#include "PostFXPlatformConfig.cginc"
		#include "UnityCG.cginc"
		#include "PostFX.cginc"//include all helper functions
		#include "HLSLSupport.cginc" //we need to support all known runtime platforms
		
		
		
		//add check for split screen 
		// if (enable_splitscreen == true)
		// {
		// 	if (invert_splitscreen == false)
		// 	{
		// 		return (IN.txcoord.x > (splitscreen_pos / 100)) ? tex2D (SamplerColor, IN.txcoord) : res;
		// 	}

		// 	if (invert_splitscreen == true)
		// 	{
		// 		return (IN.txcoord.x < (splitscreen_pos / 100)) ? tex2D (SamplerColor, IN.txcoord) : res;
		// 	}
		// }


 
		kFloat4 frag (v2f_img i) : SV_Target
		{
			//make it gray! Should work well on only with new light and gamma light space. However in other settings, looks like everything is working fine, :o
			kFloat4 original = tex2D(_MainTex, i.uv);
			
			kFloat4 processedColor = original;

			#if defined(_ScanlineAndFilmGrainON)
				kFloat x = i.uv.x * i.uv.y * (_Time[0] * 10000 * 100);//looks like unity's time is too slow, so we multiply by additional 10,000. Speed is important, otherwise u could see some wave artifact
				x = fmod(x, 13) * fmod(x, 123);	
				kFloat dx = fmod(x, 0.01);
				kFloat3 cResult = original.rgb + original.rgb * saturate(0.1f + dx.xxx * 100);

				kFloat2 sc; sincos(i.uv.y * _ScanlineCount, sc.x, sc.y);
				cResult += original.rgb * kFloat3(sc.x, sc.y, sc.x) * _ScanlineAmount;
				cResult = lerp(original, cResult, saturate(_FilmgrainAmount));
				processedColor = kFloat4(cResult.rgb, 1);
			#endif		
			
			#if defined(_GrayScaleON)
				kFloat grayscale; 
				grayscale = dot(processedColor.rgb, unity_ColorSpaceLuminance.rgb);
				kFloat4 grayedColor = kFloat4(grayscale, grayscale, grayscale, original.a);
				processedColor = grayedColor;
			#endif

			#if defined(_VignettingON)
				kFloat d = distance(i.uv.xy, kFloat2(0.5,0.5));
				processedColor *= smoothstep(_VignetteDirX, _VignetteDirY, d);
				kFloat4 vignettedCol = kFloat4(processedColor.rgb,1.0);
				processedColor = vignettedCol;
			#endif
			
			#if defined(_SepiaON)
				//-------Color Matrices for Color Correction--------------
				KFloat4x4 gray = {0.299,0.587,0.184,0,
				      0.299,0.587,0.184,0,
				      0.299,0.587,0.184,0,
				      0,0,0,1};
				      
				      
				KFloat4x4 sepia = {0.299,0.587,0.184,0.1,
				      0.299,0.587,0.184,0.018,
				      0.299,0.587,0.184,-0.090,
				      0,0,0,1};
				      
				KFloat4x4 sepia2 = {0.299,0.587,0.184,-0.090,
				      0.299,0.587,0.184,0.018,
				      0.299,0.587,0.184,0.1,
				      0,0,0,1};
				      
				KFloat4x4 sepia3 = {0.299,0.587,0.184,-0.090,
				      0.299,0.587,0.184,0.1,
				      0.299,0.587,0.184,0.1,
				      0,0,0,1};
				KFloat4x4 sepia4 = {0.299,0.587,0.184,-0.090,
				      0.299,0.587,0.184,0.018,
				      0.1299,0.587,0.184,0.1,
				      0,0,0,1};
				      
				//---------------------------------------------------------------
				
				

				kFloat3 Monochrome = (processedColor.r * 0.3f + processedColor.g * 0.59f + processedColor.b * 0.11f);//cResult will be replaced by processed color
				kFloat4 monochrome4 = kFloat4(Monochrome,1);

				
				kFloat4 result2 = processedColor;

				kFloat4 brownSepiaCol = mul(sepia,result2);
			    kFloat4 greenSepiaCol = mul(sepia3,result2);
			    kFloat4 blueSepiaCol = mul(sepia2,result2);
			    
				// return with source alpha
				kFloat4 combined =  (_brownsepia * brownSepiaCol) + (_greensepia * greenSepiaCol )+ (_bluesepia * blueSepiaCol );//+ (monochrome4 * fixed4(Monochrome.rgb, 1.0));//+(Color * result2);
				processedColor = combined;
			#endif
			
			#if defined(_BrightnessContrast)
				processedColor = processedColor * _Contrast + _Brightness;
			#endif
			
			#if defined(_LUTcolorGrading)//use lut with caution

				kFloat4 gradedPixel = sampleAs3DTexture(_LutTex, processedColor.rgb, 16);
				gradedPixel.a = processedColor.a;
				processedColor = gradedPixel;
			#endif
			

			#if defined(_Level)
				//levels input range
				processedColor.rgb = min(max(processedColor.rgb - kFloat3(_MinLevelInput, _MinLevelInput, _MinLevelInput), kFloat3(0.0, 0.0, 0.0)) / 
					(kFloat3(_MaxLevelInput, _MaxLevelInput, _MaxLevelInput) - kFloat3(_MinLevelInput, _MinLevelInput, _MinLevelInput)), kFloat3(1.0, 1.0, 1.0));

				//gamma correction
				processedColor.rgb = pow(processedColor.rgb, kFloat3(_GammaLevel, _GammaLevel, _GammaLevel));

				//levels output range
				processedColor.rgb = lerp(kFloat3(_MinLevelOutput, _MinLevelOutput, _MinLevelOutput), kFloat3(_MaxLevelOutput, _MaxLevelOutput, _MaxLevelOutput), processedColor.rgb);
			#endif

			#if defined(_ScreenBurn)
				kFloat4 BurnBasecolor = tex2D(_BeforeBurnTex, i.uv);
				kFloat BurnValue = tex2D(_BurnTex, i.uv).r;
				if(BurnValue > _burnThreshold)
				{
					processedColor = BurnBasecolor;
				}
			#endif

			#if defined(_Invert)
				processedColor.rgb = 1 - processedColor.rgb;
			#endif

			//processedColor.r = tex1D(_OneDimLUTtex, processedColor.r).r;
			//processedColor.g = tex1D(_OneDimLUTtex, processedColor.g).g;
			//processedColor.b = tex1D(_OneDimLUTtex, processedColor.b).b;

			#if defined(_colorBalance)
				kFloat lightness = RGBToL(processedColor.rgb);
				kFloat3 shift = kFloat3(_redShift, _greenShift, _blueShift);

				kFloat a = 0.25;
				kFloat b = 0.333;
				kFloat scale = 0.7;

				kFloat3 midtones = (clamp((lightness - b) /  a + 0.5, 0.0, 1.0) * clamp ((lightness + b - 1.0) / -a + 0.5, 0.0, 1.0) * scale) * shift;

				kFloat3 newColor = processedColor.rgb + midtones;
				newColor = clamp(newColor, 0.0, 1.0);

				// preserve luminosity
				kFloat3 newHSL = RGBToHSL(newColor);
				kFloat oldLum = RGBToL(processedColor.rgb);
				processedColor.rgb = HSLToRGB(kFloat3(newHSL.x, newHSL.y, oldLum));
			#endif

			#if defined(_Vibrance)
				kFloat3 luma_coef = kFloat3 (0.212656, 0.715158, 0.072186);		// Calculate luma with these values
				//in case of auto saturation
				// fixed max_color = max (processedColor.r, max (processedColor.g, processedColor.b));	// Find the strongest color
				// fixed min_color = min (processedColor.r, min (processedColor.g, processedColor.b));	// Find the weakest color
				//fixed color_saturation = max_color - min_color;				// Saturation is the difference between min and max
				kFloat luma = dot (luma_coef, processedColor.rgb);				// Calculate luma (grey)

				// Extrapolate between luma and processedColor by 1 + (1 - saturation) - current
				processedColor.rgb = lerp (luma.rrr, processedColor.rgb, (1.0 + (_VibranceAmount * (1.0 - (sign (_VibranceAmount) * _SaturationAmount)))));
			#endif


			#if defined(_Sharp)
				processedColor = SharpSceneImage(_MainTex, i.uv, processedColor);
			#endif
			

			#if defined(_Exposure)

				#if POSTFX_EXPOSURE_CHEAP
				kFloat gamma = 1;
				gamma = 1/_GammaCorrectionExposure;

				//calculate exposure 
				//http://www.francois-tarlier.com/blog/exposureoffsetgamma-photoshop-tool-to-shaders/
				processedColor *= _ExposureAmount;
	           //DRAW Original
			   //Exposure + Luminance (offset) + gamma if we agree here, or render original
				processedColor.rgb += kFloat3(_OffsetExposure, _OffsetExposure, _OffsetExposure);
				processedColor.rgb = pow(processedColor.rgb, gamma);

				#else
				//run expensive algorithm
				//calculate exposure
				processedColor *= _ExposureAmount;

				//convert to YCbCr
				kFloat Y = 0.299 * processedColor.r + 0.587 * processedColor.g + 0.114 * processedColor.b;
				kFloat Cb = -0.1687 * processedColor.r + -0.3313 * processedColor.g + 0.5 * processedColor.b + 0.5;
				kFloat Cr = 0.5 * processedColor.r - 0.4187 * processedColor.g - 0.0813 * processedColor.b + 0.5;

				//calculate luminance
				Y = Y+ _OffsetExposure;


				//convert back to RGB
				kFloat R = Y + 1.402 * (Cr - 0.5);
				kFloat G = Y - 0.34414 * (Cb - 0.5) - 0.71414 * (Cr - 0.5);
				kFloat B = Y + 1.772 * (Cb - 0.5);
				processedColor = kFloat4(R,G,B,1);
				#endif
			#endif

			#if defined(_ChannelMixer)
				kFloat3 lumcoeff = kFloat3(0.299,0.587,0.114);
				kFloat3 redchannel = processedColor.rrr * _RedAmountChannelMixer;
				kFloat3 greenchannel = processedColor.ggg * _GreenAmountChannelMixer;
				kFloat3 bluechannel = processedColor.bbb * _BlueAmountChannelMixer;
				kFloat3 result = redchannel + greenchannel + bluechannel;
				kFloat dotAmount = dot(processedColor.rgb, lumcoeff);
				kFloat3 graychannel = kFloat3(dotAmount, dotAmount, dotAmount) * (1.0 + _GrayAmountChannelMixer);
				kFloat averageAmount = (graychannel.r + graychannel.g + graychannel.b)/3.0;
				graychannel = kFloat3(averageAmount, averageAmount, averageAmount);
				processedColor = kFloat4(lerp(result, graychannel, _Desaturation), processedColor.a);
			#endif

			#if defined(_Fade)
				processedColor = lerp(processedColor, _FadeColor, _FadeAmount);
			#endif

			#if defined(_twirl)
				// kFloat2 point = i.uv.xy/_MainTex_TexelSize.xy;	//normalize coordinates
				// kFloat2 normCoord = kFloat2(2.0, 2.0) * point.xy - kFloat2(1.0, 1.0);
				kFloat2 normCoord += _TwirlOrigin.xy;
				float r = length(normCoord);
				float phi = atan2(normCoord.y, normCoord.x); 
				// now maipulate r and phi to do your bidding.
				phi = phi + (1.0 - smoothstep(-_TwirlSize, _TwirlSize,r)) * _TwirlAmount;
				// back from polar space.
				normCoord.x = r* cos(phi);
				normCoord.y = r* sin(phi);

				normCoord -= _TwirlOrigin;

				kFloat2 modedUV = normCoord; //(normCoord / 2.0 + 0.5) * _MainTex_TexelSize.xy;
				processedColor = tex2D(_MainTex, modedUV);
			#endif

			return processedColor;

		}
		

		ENDCG

	}
}

Fallback off

}
