﻿///Credits:
///For film grain, scanline and sepia color correction: 
/////http://forum.thegamecreators.com/?m=forum_view&b=24&t=172965&p=0### bond1 from thegamecreators.com, thank you!
///as it is still working in progress, proper credit text will be managed and placed appropriately.

Shader "Kaiyum/Bloom" {
Properties {
	_MainTex ("Screen Texture(populated by engine)", 2D) = "white" {}

}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
				
		CGPROGRAM
		#pragma vertex vert_img
		#pragma fragment frag
		#pragma target 3.0
		#include "UnityCG.cginc"
		#include "HLSLSupport.cginc" //we need to support all known runtime platforms
        uniform half delta = 0.25, bloomRev;

		uniform float4 _MainTex_TexelSize;
        uniform sampler2D _MainTex;
 
		fixed4 frag (v2f_img i) : SV_Target
		{
			//make it gray! Should work well on only with new light and gamma light space. However in other settings, looks like everything is working fine, :o
			fixed4 original = tex2D(_MainTex, i.uv);
			float len = original.r + original.g + original.b / 3;// length(original);
			fixed4 processedColor = original;

            half2 uv1 = i.uv + _MainTex_TexelSize.xy * half2(delta, delta);
            half2 uv2 = i.uv + _MainTex_TexelSize.xy * half2(-delta, delta);
            half2 uv3 = i.uv + _MainTex_TexelSize.xy * half2(delta, -delta);
            half2 uv4 = i.uv + _MainTex_TexelSize.xy * half2(-delta, -delta);

            fixed4 c1 = tex2D(_MainTex, uv1);
			fixed4 c2 = tex2D(_MainTex, uv2);
            fixed4 c3 = tex2D(_MainTex, uv3);
            fixed4 c4 = tex2D(_MainTex, uv4);
            fixed4 avgCol = ( c1 + c2 + c3 + c4) / 4;
            
            fixed4 finalCol = original + avgCol * length(original) * bloomRev;
            
			return finalCol;


		}
		

		ENDCG

	}
}

Fallback off

}
